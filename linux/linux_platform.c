/* normals */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

/* x related */
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <X11/Xlib.h>

/* needed for memcpy */
#include <string.h>

#define VK_USE_PLATFORM_XCB_KHR
#include <vulkan/vulkan.h>

#include "common.h"
#include "platform.h"

void HandleKey(Input*, uint8_t, int);

typedef struct WindowInfo
{
  xcb_connection_t *conn;
  xcb_window_t window_id;
  xcb_void_cookie_t window_cookie;
  const xcb_setup_t *setup;
  xcb_screen_iterator_t iter;
  xcb_screen_t *screen;

  int width, height;
  int win_width, win_height;
} WindowInfo;

const char* vulkan_instance_extension_names[] =
  {
   VK_KHR_SURFACE_EXTENSION_NAME,
   VK_KHR_XCB_SURFACE_EXTENSION_NAME
  };

const uint32_t vulkan_instance_extension_num = SIZE_ARRAY( vulkan_instance_extension_names );

static WindowInfo winfo;
// static Input* winput; // static_input in windows_platform.c
time_t start_time;

static void init_xcb_win(WindowInfo *winfo,
                         uint16_t width, uint16_t height,
                         const char *title)
{
  winfo->width = width;
  winfo->height = height;

  winfo->conn =
    xcb_connect(NULL,           /* use env DISPLAY */
                NULL            /* discard screen number */
                );

  winfo->window_id = xcb_generate_id(winfo->conn);
  winfo->setup = xcb_get_setup(winfo->conn);
  winfo->iter = xcb_setup_roots_iterator(winfo->setup);
  winfo->screen = winfo->iter.data;


  xcb_event_mask_t valwin[] =
    {
     XCB_EVENT_MASK_KEY_PRESS |
     XCB_EVENT_MASK_KEY_RELEASE |
     XCB_EVENT_MASK_BUTTON_PRESS |
     XCB_EVENT_MASK_BUTTON_RELEASE |
     XCB_EVENT_MASK_POINTER_MOTION
    };

  winfo->window_cookie =
    xcb_create_window(winfo->conn,
                      XCB_COPY_FROM_PARENT, /* depth */
                      winfo->window_id,
                      winfo->screen->root, /* parent */
                      100, /* x */
                      100, /* y */
                      width, /* width */
                      height, /* height */
                      1,   /* border width */
                      XCB_WINDOW_CLASS_INPUT_OUTPUT, /* _class */
                      winfo->screen->root_visual, /* visual */
                      XCB_CW_EVENT_MASK, /* value_mask */
                      valwin /* value_list */);

  xcb_map_window(winfo->conn, winfo->window_id);
  xcb_flush(winfo->conn);
}

WindowInfo* InitializeWindowInfo( const char* name, uint32_t width, uint32_t height )
{
  init_xcb_win(&winfo, width, height, name);
  time(&start_time);
  return &winfo;
}

void DestroyWindowInfo( WindowInfo* window )
{
  // DO NOTHING
  return;
}

// Timing functions
// pick a point of time from zero, return ticks since that time
void DoWindowRenderUpdate( WindowInfo* window )
{
  return;
}


// Event functions
void ProcessEvents( Input* winput )
{
  xcb_key_symbols_t *theKeySyms = xcb_key_symbols_alloc(winfo.conn);

  // unhandled on winput right now:
  /* int pause; */
  /* int quit; */
  // todo: map the above to specific keys?

  xcb_generic_event_t *e;
  while(1){
    e = xcb_poll_for_event(winfo.conn);
    if(e == NULL){
      if(xcb_connection_has_error(winfo.conn)){
        exit(EXIT_SUCCESS);
      }else{
        /* //logg("e was null."); */
        break;
      }
    }else{
      switch(e->response_type){

      case XCB_KEY_PRESS: {
        xcb_key_press_event_t *ev = (xcb_key_press_event_t *)e;
        xcb_keysym_t theKeySym = xcb_key_press_lookup_keysym(theKeySyms, ev, 1);
        HandleKey(winput, ev->detail, 1);
        break;
      }

      case XCB_KEY_RELEASE:{
        xcb_key_release_event_t *ev = (xcb_key_release_event_t *)e;
        xcb_keysym_t theKeySym = xcb_key_release_lookup_keysym(theKeySyms, ev, 1);
        HandleKey(winput, ev->detail, 0);
        break;
      }

      case XCB_BUTTON_PRESS:{
        xcb_button_press_event_t *ev = (xcb_button_press_event_t *)e;
        /* HandleMouse(winput, ev->detail, 1); */
        break;
      }

      case XCB_BUTTON_RELEASE:{
        xcb_button_release_event_t *ev = (xcb_button_release_event_t *)e;
        /* HandleMouse(winput, ev->detail, 0); */
        break;
      }

      case XCB_MOTION_NOTIFY: {
        xcb_motion_notify_event_t *motion = (xcb_motion_notify_event_t *)e;
        winput->mousex = motion->event_x;
        winput->mousey = motion->event_y;
        break;
      }

      default:
        break;
      }
    }}
  return;
}

int CreateWindowSurface( const VkInstance vk_inst,
                         const WindowInfo* window,
                         VkSurfaceKHR* surf )
{
  VkXcbSurfaceCreateInfoKHR surface_create_info = { 0 };
  /* surface_create_info.sType = VK_STRUCTURE_TYPE_MIR_SURFACE_CREATE_INFO_KHR; */
  /* surface_create_info.pNext; */
  /* surface_create_info.flags; */
  surface_create_info.connection = window->conn;
  surface_create_info.window = window->window_id;
  VkResult res = vkCreateXcbSurfaceKHR( vk_inst,
                                        &surface_create_info,
                                        NULL,
                                        surf);
  surface_create_info.pNext = NULL;
  if ( res )
    {
      init_log( "Failed to create a vulkan surface." );
      exit( 1 );
    }
  else
    {
      init_log( "Successfully created vulkan rendering surface." );
    }
  return res;
}


int ReadBinaryFile( FILE *fp, uint32_t* buf, size_t bufsize, size_t* readsize )
{
#define KB 1024
#define MB (1024 * KB)

  size_t total_read = 0;
  size_t actual_read;
  do
    {
      // Read the file in 1 MB chunks.
      size_t to_read = MB > bufsize - total_read ? bufsize - total_read : MB;

      actual_read= fread(buf + total_read, 1, to_read, fp);
      if ( actual_read != to_read )
        {
          // Uh-oh an error occurred.
          *readsize = total_read;
          /* logg("failed reading binary file."); */
          /* logg("[%zu] != [%zu]", actual_read, to_read); */
          return 1;
        }else{
        /* logg("[%zu] == [%zu]", actual_read, to_read); */
      }

      total_read += actual_read;
    } while ( actual_read != 0 && total_read < bufsize );

  *readsize = total_read;
  return 0;
}

size_t CalculateFileSize( FILE *fp )
{
  fseek(fp, 0L, SEEK_END);
  size_t size = ftell(fp);
  fseek(fp, 0L, SEEK_SET);

  return size;
}

uint32_t* ReadBinaryFileMalloc( const char* filename, size_t* size )
{
  /* HANDLE hfile = CreateFile( filename, */
  /*     GENERIC_READ, */
  /*     FILE_SHARE_READ, */
  /*     NULL, */
  /*     OPEN_EXISTING, */
  /*     FILE_ATTRIBUTE_NORMAL, */
  /*     NULL ); */

  FILE *fp = fopen(filename, "rb");

  if ( fp == NULL ){
    /* logg("open failed."); */
    return NULL;
  }else{
    /* logg("opened the shader fine."); */
  }

  size_t file_size = CalculateFileSize( fp );
  /* logg("file_size = %zu", file_size); */
  size_t u32_size = file_size / sizeof( uint32_t ) + !!(file_size % sizeof( uint32_t ));
  size_t total_size = u32_size * sizeof( uint32_t );
  uint32_t* result = malloc( total_size );
  /* logg("malloc'd %zu bytes.", total_size); */
  if ( !result ){
    return NULL;
  }

  ReadBinaryFile( fp, result, total_size, size );

  fclose( fp );

  return result;
}

// Timing functions
// number of ticks since start
uint64_t GetTicks()
{
  return clock();
}

// want to return number of ticks per second
uint64_t GetTickFrequency()
{
  return CLOCKS_PER_SEC / 14;
}

/* implemented */

uint32_t GetWidth( const WindowInfo* window )
{
  return window->width;
}

uint32_t GetHeight( const WindowInfo* window )
{
  return window->height;
}

/* util functions */

// todo: track/handle multiple buttons
void HandleMouse(Input* winput, int button, int pressed) {
  if (button == 1) {
    /* winput->mousestate = pressed ? MOUSE_LDOWN : MOUSE_LUP; */
  } else if (button == 2) {
    /* winput->mousestate = pressed ? MOUSE_RDOWN : MOUSE_RUP; */
  }
}

void HandleKey(Input* winput, uint8_t key, int state)
{
  uint8_t playerKey;
  switch(key){
  case 40: //d
  case 114://right
    playerKey = KeyRight; break;
  case 39: // s
  case 116://down
    playerKey = KeyDown; break;
  case 38: // a
  case 113://left
    playerKey = KeyLeft; break;
  case 25: // w
  case 111://up
    playerKey = KeyUp; break;
  case 24: //q
    winput->quit = 1;
  default:
    /* logg("unknown key: %d", key); */
    break;
  }

  winput->keydown[playerKey] = state;
}
