
#ifndef MODEL_H
#define MODEL_H

#include "vulkan_wrapper.h"

ColorVertex* ExtrudeModel( const ColorVertex* front_face, uint32_t face_count,
                           const ColorVertex* front_edge, uint32_t edge_count,
                           float relative_z, uint32_t* result_count );

void CalculateNormals( const ColorVertex* vertices, uint32_t triangle_count, Vector3* normals );

void ScaleModel( ColorVertex* vertices, uint32_t count, float scale );

void StretchModel( ColorVertex* vertices, uint32_t count, Vector3 stretch );

void TranslateModel( ColorVertex* vertices, uint32_t count, Vector3 offset );

void PrintModel( const ColorVertex* vertices, uint32_t count );

ColorVertex* GenerateHawk( float width, float height, uint32_t* result_count );
ColorVertex* GenerateBug( float width, float height, uint32_t* result_count );
ColorVertex* GenerateBuilding( float width, float height, uint32_t* result_count );

#endif
