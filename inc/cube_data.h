
#ifndef CUBE_DATA_H
#define CUBE_DATA_H

#include "vulkan_wrapper.h"

typedef struct VertexUV
{
    float X, Y, Z, W, U, V;
} VertexUV;

#define XYZ1(_x_, _y_, _z_) (_x_), (_y_), (_z_), 1.f
#define UV(_u_, _v_) (_u_), (_v_)

const ColorVertex g_vbData[] = {
    { XYZ1( -1, -1, -1 ), XYZ1( 0.f, 0.f, 0.f ) },{ XYZ1( 1, -1, -1 ), XYZ1( 1.f, 0.f, 0.f ) },{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },{ XYZ1( 1, -1, -1 ), XYZ1( 1.f, 0.f, 0.f ) },{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },

{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },{ XYZ1( -1, 1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },
{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },{ XYZ1( -1, 1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },{ XYZ1( 1, 1, 1 ), XYZ1( 1.f, 1.f, 1.f ) },

{ XYZ1( 1, 1, 1 ), XYZ1( 1.f, 1.f, 1.f ) },{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },
{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },{ XYZ1( 1, -1, -1 ), XYZ1( 1.f, 0.f, 0.f ) },

{ XYZ1( -1, 1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },{ XYZ1( -1, -1, -1 ), XYZ1( 0.f, 0.f, 0.f ) },

{ XYZ1( 1, 1, 1 ), XYZ1( 1.f, 1.f, 1.f ) },{ XYZ1( -1, 1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },
{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },{ XYZ1( -1, 1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },

{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },{ XYZ1( 1, -1, -1 ), XYZ1( 1.f, 0.f, 0.f ) },{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },
{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },{ XYZ1( 1, -1, -1 ), XYZ1( 1.f, 0.f, 0.f ) },{ XYZ1( -1, -1, -1 ), XYZ1( 0.f, 0.f, 0.f ) },
};

const ColorVertex g_vb_solid_face_colors_Data[] = {
    // red face
    { XYZ1( -1, -1, 1 ), XYZ1( 1.f, 0.f, 0.f ) },
{ XYZ1( -1, 1, 1 ), XYZ1( 1.f, 0.f, 0.f ) },
{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 0.f, 0.f ) },
{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 0.f, 0.f ) },
{ XYZ1( -1, 1, 1 ), XYZ1( 1.f, 0.f, 0.f ) },
{ XYZ1( 1, 1, 1 ), XYZ1( 1.f, 0.f, 0.f ) },
// green face
{ XYZ1( -1, -1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
{ XYZ1( 1, -1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
{ XYZ1( 1, -1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
{ XYZ1( 1, 1, -1 ), XYZ1( 0.f, 1.f, 0.f ) },
// blue face
{ XYZ1( -1, 1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },
{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },
{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 0.f, 1.f ) },
{ XYZ1( -1, 1, -1 ), XYZ1( 0.f, 0.f, 1.f ) },
{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 0.f, 1.f ) },
{ XYZ1( -1, -1, -1 ), XYZ1( 0.f, 0.f, 1.f ) },
// yellow face
{ XYZ1( 1, 1, 1 ), XYZ1( 1.f, 1.f, 0.f ) },
{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },
{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 1.f, 0.f ) },
{ XYZ1( 1, -1, 1 ), XYZ1( 1.f, 1.f, 0.f ) },
{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },
{ XYZ1( 1, -1, -1 ), XYZ1( 1.f, 1.f, 0.f ) },
// magenta face
{ XYZ1( 1, 1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },
{ XYZ1( -1, 1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },
{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 0.f, 1.f ) },
{ XYZ1( 1, 1, -1 ), XYZ1( 1.f, 0.f, 1.f ) },
{ XYZ1( -1, 1, 1 ), XYZ1( 1.f, 0.f, 1.f ) },
{ XYZ1( -1, 1, -1 ), XYZ1( 1.f, 0.f, 1.f ) },
// cyan face
{ XYZ1( 1, -1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },
{ XYZ1( 1, -1, -1 ), XYZ1( 0.f, 1.f, 1.f ) },
{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },
{ XYZ1( -1, -1, 1 ), XYZ1( 0.f, 1.f, 1.f ) },
{ XYZ1( 1, -1, -1 ), XYZ1( 0.f, 1.f, 1.f ) },
{ XYZ1( -1, -1, -1 ), XYZ1( 0.f, 1.f, 1.f ) },
};

const VertexUV g_vb_texture_Data[] = {
    // left face
    { XYZ1( -1, -1, -1 ), UV( 1.f, 0.f ) },  // lft-top-front
{ XYZ1( -1, 1, 1 ), UV( 0.f, 1.f ) },    // lft-btm-back
{ XYZ1( -1, -1, 1 ), UV( 0.f, 0.f ) },   // lft-top-back
{ XYZ1( -1, 1, 1 ), UV( 0.f, 1.f ) },    // lft-btm-back
{ XYZ1( -1, -1, -1 ), UV( 1.f, 0.f ) },  // lft-top-front
{ XYZ1( -1, 1, -1 ), UV( 1.f, 1.f ) },   // lft-btm-front
                                         // front face
{ XYZ1( -1, -1, -1 ), UV( 0.f, 0.f ) },  // lft-top-front
{ XYZ1( 1, -1, -1 ), UV( 1.f, 0.f ) },   // rgt-top-front
{ XYZ1( 1, 1, -1 ), UV( 1.f, 1.f ) },    // rgt-btm-front
{ XYZ1( -1, -1, -1 ), UV( 0.f, 0.f ) },  // lft-top-front
{ XYZ1( 1, 1, -1 ), UV( 1.f, 1.f ) },    // rgt-btm-front
{ XYZ1( -1, 1, -1 ), UV( 0.f, 1.f ) },   // lft-btm-front
                                         // top face
{ XYZ1( -1, -1, -1 ), UV( 0.f, 1.f ) },  // lft-top-front
{ XYZ1( 1, -1, 1 ), UV( 1.f, 0.f ) },    // rgt-top-back
{ XYZ1( 1, -1, -1 ), UV( 1.f, 1.f ) },   // rgt-top-front
{ XYZ1( -1, -1, -1 ), UV( 0.f, 1.f ) },  // lft-top-front
{ XYZ1( -1, -1, 1 ), UV( 0.f, 0.f ) },   // lft-top-back
{ XYZ1( 1, -1, 1 ), UV( 1.f, 0.f ) },    // rgt-top-back
                                         // bottom face
{ XYZ1( -1, 1, -1 ), UV( 0.f, 0.f ) },  // lft-btm-front
{ XYZ1( 1, 1, 1 ), UV( 1.f, 1.f ) },    // rgt-btm-back
{ XYZ1( -1, 1, 1 ), UV( 0.f, 1.f ) },   // lft-btm-back
{ XYZ1( -1, 1, -1 ), UV( 0.f, 0.f ) },  // lft-btm-front
{ XYZ1( 1, 1, -1 ), UV( 1.f, 0.f ) },   // rgt-btm-front
{ XYZ1( 1, 1, 1 ), UV( 1.f, 1.f ) },    // rgt-btm-back
                                        // right face
{ XYZ1( 1, 1, -1 ), UV( 0.f, 1.f ) },   // rgt-btm-front
{ XYZ1( 1, -1, 1 ), UV( 1.f, 0.f ) },   // rgt-top-back
{ XYZ1( 1, 1, 1 ), UV( 1.f, 1.f ) },    // rgt-btm-back
{ XYZ1( 1, -1, 1 ), UV( 1.f, 0.f ) },   // rgt-top-back
{ XYZ1( 1, 1, -1 ), UV( 0.f, 1.f ) },   // rgt-btm-front
{ XYZ1( 1, -1, -1 ), UV( 0.f, 0.f ) },  // rgt-top-front
                                        // back face
{ XYZ1( -1, 1, 1 ), UV( 1.f, 1.f ) },   // lft-btm-back
{ XYZ1( 1, 1, 1 ), UV( 0.f, 1.f ) },    // rgt-btm-back
{ XYZ1( -1, -1, 1 ), UV( 1.f, 0.f ) },  // lft-top-back
{ XYZ1( -1, -1, 1 ), UV( 1.f, 0.f ) },  // lft-top-back
{ XYZ1( 1, 1, 1 ), UV( 0.f, 1.f ) },    // rgt-btm-back
{ XYZ1( 1, -1, 1 ), UV( 0.f, 0.f ) },   // rgt-top-back
};

#endif
