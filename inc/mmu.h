#ifndef MMU_H
#define MMU_H

#include "gamestate.h"

// when you add to this, add in the middle (BUG and HAWK are bound checks)

struct NPC *mmu_alloc(enum obj_type type);
void mmu_free(struct NPC *o);

#endif // MMU_H
