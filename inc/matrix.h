
#ifndef MATRIX_H
#define MATRIX_H

#include <math.h>

typedef struct Vector3
{
    float x, y, z;
} Vector3;

typedef struct Vector4
{
    float x, y, z, w;
} Vector4;

typedef struct Matrix4 
{
    Vector4 vec[4];
} Matrix4;

Vector3 VectAdd( Vector3 a, Vector3 b );
Vector3 VectSub( Vector3 a, Vector3 b );
Vector3 VectScale( Vector3 a, float scale );
float VectDot( Vector3 a, Vector3 b );
float VectLengthSq( Vector3 vect );
float VectLength( Vector3 vect );
Vector3 VectNormalize( Vector3 vect );
Vector3 VectCross( Vector3 a, Vector3 b );
Matrix4 Mult4( Matrix4 a, Matrix4 b );
Matrix4 GetDiagonalMatrix( float scalar );
Matrix4 GetProjection( float fov, float aspect, float zNear, float zFar );
Matrix4 GetProjection2D( float left, float right, float bottom, float top,
                         float near_z, float far_z );
Matrix4 LookAt( Vector3 eye,
                Vector3 center,
                Vector3 up );
Matrix4 GetTranslation( float dx, float dy, float dz );
Matrix4 CopyTranslation( Matrix4 other );
Matrix4 GetRotation( float x_theta, float y_theta, float z_theta );
#endif
