#ifndef PLATFORM_H
#define PLATFORM_H

#include "platform_vulkan.h"
#include "gamestate.h"

// Window functions
WindowInfo* InitializeWindowInfo( const char* name, uint32_t width, uint32_t height );
void DestroyWindowInfo( WindowInfo* window );

uint32_t GetWidth( const WindowInfo* window );
uint32_t GetHeight( const WindowInfo* window );

void DoWindowRenderUpdate( WindowInfo* window );

// Event functions
void ProcessEvents( Input* input );

// Timing functions
uint64_t GetTicks();
uint64_t GetTickFrequency();

// File I/O
uint32_t* ReadBinaryFileMalloc( const char* filename, size_t* size );
size_t ReadFileSize( const char* filename );

#endif
