
#ifndef PLATFORM_VULKAN_H
#define PLATFORM_VULKAN_H

#ifdef WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#endif

#include "vulkan/vulkan.h"

// This file includes platform-specific vulkan APIs.

typedef struct WindowInfo WindowInfo;

extern const char* vulkan_instance_extension_names[];
extern const uint32_t vulkan_instance_extension_num;

int CreateWindowSurface( const VkInstance vk_inst,
                         const WindowInfo* window,
                         VkSurfaceKHR* surf );

#endif