
#ifndef VULKAN_WRAPPER_H
#define VULKAN_WRAPPER_H

// This file declares functions and structs which wrap window initialization and vulkan setup for the game.
#include "stdint.h"

// Platform include must come before vulkan include
#include "platform_vulkan.h"
#include "vulkan/vulkan.h"

#define LOG_QUIET 0
#define LOG_NORMAL 1
#define LOG_VERBOSE 2

#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_NORMAL
#endif

#if LOG_LEVEL == LOG_QUIET
#define init_log( ... )
#define render_log( ... )
#elif LOG_LEVEL == LOG_NORMAL
#define init_log( ... ) printf( __VA_ARGS__ )
#define render_log( ... )
#else // Verbose
#define init_log( ... ) printf( __VA_ARGS__ )
#define render_log( ... ) printf( __VA_ARGS__ )
#endif

typedef struct VulkanImage
{
    VkFormat format;
    VkImage image;
    VkImageView view;
    VkDeviceMemory mem;
} VulkanImage;

typedef struct VulkanBuffer
{
    VkBuffer buffer;
    VkDeviceMemory mem;
    VkDescriptorBufferInfo descriptor_info;
    size_t alignment;
    int is_dynamic;
} VulkanBuffer;

typedef struct VulkanShader
{
    uint32_t* spv;
    VkPipelineShaderStageCreateInfo stage_create;
} VulkanShader;

typedef struct VulkanSwapchain
{
    VkSwapchainKHR swapchain;
    VkImage* images;
    VkImageView* views;
    uint32_t count;
    VkFormat format;
} VulkanSwapchain;

typedef struct RenderRect
{
    uint32_t x, y;
    uint32_t width, height;
} RenderRect;

typedef struct ColorVertex
{
    float x, y, z, w, r, g, b, a;
} ColorVertex;

// Utility functions. See: vulkan_util.c
void WaitForFenceIndefinite( const VkDevice dev, const VkFence wait_for );

int GetMemoryTypeFromDeviceProperties( const VkPhysicalDeviceMemoryProperties* properties, 
                                       uint32_t type_bits, 
                                       VkFlags req_mask, 
                                       uint32_t* type_index );

// This function needs to be called to "attach" the uniform
// buffer to the descriptor set. This needs to be done
// once each time the uniform buffer is (re)allocated.
void UpdateDescriptorSet( const VkDevice dev,
                          const VkDescriptorSet descriptor_set,
                          const VulkanBuffer* uniform_buf,
                          uint32_t destination_binding );

// Vulkan Buffer functions. See: vulkan_buffer.c
int WriteToVulkanBuffer( const VkDevice dev,
                         VulkanBuffer* buffer, 
                         const void* write_data, 
                         size_t size );

int CreateVulkanBuffer( const VkPhysicalDevice phys_dev,
                        const VkDevice dev,
                        VulkanBuffer* buffer, 
                        uint32_t usage, 
                        const void* write_data, 
                        size_t size );

void FreeVulkanBuffer( const VkDevice dev,
                       VulkanBuffer* buffer );

int CreateUniformBuffer( const VkPhysicalDevice phys_dev, 
                         const VkDevice dev,
                         size_t element_size,
                         size_t num,
                         VulkanBuffer* buf );

int ReallocUniformBuffer( const VkPhysicalDevice phys_dev,
                          const VkDevice dev,
                          size_t element_size,
                          size_t num,
                          VulkanBuffer* buf );

int WriteToUniformBuffer( const VkDevice dev,
                          VulkanBuffer* buf,
                          size_t element_size,
                          size_t num,
                          const void* write_data );

int CreateVertexBuffer( const VkPhysicalDevice phys_dev,
                        const VkDevice dev, 
                        const ColorVertex* vertices, 
                        uint32_t vertex_count,
                        VulkanBuffer* buffer );

// Vulkan image functions. See: vulkan_image.c
int CreateDepthImage( const VkPhysicalDevice phys_dev,
                      const VkDevice dev,
                      uint32_t width,
                      uint32_t height,
                      VulkanImage* depth_image );

void FreeImage( const VkDevice dev, VulkanImage* image );

// Vulkan Shader functions. See: vulkan_shader.c
int CreateVertexShader( const VkDevice dev, const char* shader_name, VulkanShader* shader );

int CreateFragmentShader( const VkDevice dev, const char* shader_name, VulkanShader* shader );

void FreeShader( const VkDevice dev, VulkanShader* shader );

// Initialization functions (in chronological order). See: vulkan_init.c
// Each function here represents a single "step" in the Vulkan initialization
// process. Where possible, each function makes simplifying assumptions.
int InitializeInstance( VkApplicationInfo* vk_info, 
                        VkInstance* inst, 
                        const char* app_name );

VkPhysicalDevice* GetPhysicalDevices( const VkInstance inst, uint32_t* count );

VkQueueFamilyProperties* GetQueueFamilies( const VkPhysicalDevice phys_dev,
                                           const VkSurfaceKHR surface,
                                           uint32_t* graphics_queue_index,
                                           uint32_t* present_queue_index,
                                           uint32_t* queue_count );

int InitializeLogicalDevice( const VkPhysicalDevice phys_dev,
                             uint32_t graphics_queue_index,
                             VkDevice* dev );

int InitializeCommandPool( const VkDevice dev,
                           uint32_t graphics_queue_index,
                           VkCommandPool* pool,
                           VkCommandBuffer* buffers,
                           uint32_t count );

int InitializeSwapchain( const VkPhysicalDevice phys_dev,
                         const VkDevice dev,
                         const VkSurfaceKHR surface,
                         uint32_t display_width,
                         uint32_t display_height,
                         uint32_t graphics_queue_index,
                         uint32_t present_queue_index,
                         VulkanSwapchain* swapchain );

int InitializeGraphicsAndPresentQueues( const VkDevice dev,
                                        uint32_t graphics_queue_index,
                                        uint32_t present_queue_index,
                                        VkQueue* graphics_queue,
                                        VkQueue* present_queue );

int InitializePipelineLayoutAndDescriptorSets( const VkDevice dev,
                                               VkDescriptorSetLayout* descriptor_layout,
                                               VkDescriptorPool* descriptor_pool,
                                               VkDescriptorSet* descriptor_set,
                                               VkPipelineLayout* pipeline_layout );

// Here, present_format is the format that is used by the swapchain
// to present to the window surface.
int InitializeRenderPass( const VkDevice dev,
                          const VulkanImage* depth_image,
                          VkFormat present_format,
                          VkRenderPass* render_pass );

int InitializeFramebuffers( const VkDevice dev,
                            const VkRenderPass render_pass,
                            const VulkanImage* depth_image,
                            const VulkanSwapchain* swapchain,
                            uint32_t width,
                            uint32_t height,
                            VkFramebuffer* framebuffers );

int InitializePipeline( const VkDevice dev,
                        const VkPipelineLayout pipeline_layout,
                        const VkRenderPass render_pass,
                        const VulkanShader* shaders,
                        uint32_t shader_count,
                        VkPipeline* pipeline );

// Rendering Functions See: vulkan_render.c
int AcquireSwapchainImage( const VkDevice dev,
                           const VulkanSwapchain* swapchain,
                           uint32_t* framebuffer_index,
                           VkSemaphore* image_acquired );

int BeginRenderRecording( const VkDevice dev,
                          const VkRenderPass render_pass,
                          const VkFramebuffer framebuffer,
                          const VkCommandBuffer commands,
                          RenderRect render_area );

int EndRenderRecording( const VkCommandBuffer commands );

int SubmitQueue( const VkDevice dev,
                 const VkQueue graphics_queue,
                 const VkCommandBuffer* command_buffers,
                 uint32_t buffer_count,
                 const VkSemaphore* acquire_sem, 
                 VkFence* draw_fence );

int PresentToDisplay( const VkDevice dev,
                      const VkQueue present_queue,
                      const VulkanSwapchain* swapchain,
                      uint32_t buffer_index,
                      const VkFence draw_fence );

#endif