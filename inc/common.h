#ifndef COMMON_H
#define COMMON_H

#ifdef __WIN32
#define __USE_MINGW_ANSI_STDIO 1
#endif

#include "stdio.h"
#include "stdint.h"
#include "renderer.h"
#include "vulkan_wrapper.h"

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080

#define MIN_SCREEN_WIDTH_PX 700
#define MIN_SCREEN_HEIGHT_PX 700

#define SIZE_ARRAY( arr ) (sizeof(arr) / sizeof((arr)[0]))

#define MOUSE_LDOWN 1
#define MOUSE_LUP 0

#include "vulkan_wrapper.h"

typedef struct RGB
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
} RGB;

typedef enum Quadrant
{
    INVALID = 0,
    TOP_RIGHT = 1,
    TOP_LEFT = 2,
    BOTTOM_LEFT = -2, // Negative values used only to provide means of determining if quadrant is above or below x axis
    BOTTOM_RIGHT = -1
} Quadrant;

void ValidateObject(void* ptr, char* name);

float AbsoluteValue( float val );
float Exponentiate( float base, float exponent );
float NthRoot( int nth_root, float val );

void create_model_post_extruded(struct model *m,
                                ColorVertex *verts,
                                uint32_t offset);
void create_model(struct model *m,
                  ColorVertex *front_face, uint32_t face_len,
                  ColorVertex *front_edge, uint32_t edge_len,
                  float extrude_height,
                  uint32_t offset);

void set_entity_position(Matrix4 *m,
                         float x,
                         float y,
                         float z);
void set_entity_rotation(Matrix4 *m,
                         float a,
                         float b,
                         float t);

uint32_t main_give_me_the_offset_plox(uint32_t in_off);

void set_matrix(uint32_t c, Matrix4 m);

#endif //COMMON_H
