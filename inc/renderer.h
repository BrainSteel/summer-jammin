#ifndef RENDERER_H
#define RENDERER_H

#include "matrix.h"
#include "vulkan_wrapper.h"

struct model{
  VulkanBuffer vbuff;
  uint32_t vcount;
  VkCommandBuffer command_buffer;
  VkPipelineLayout pipeline_layout;
  VkDescriptorSet descriptor_set;
  uint32_t uniform_offset;

  Matrix4 matrix;
};

typedef struct Point
{
    float x_meter;
    float y_meter;
} Point;

void render_model(struct model *m);
void render_model_pos(struct model *m, Point coords);
void render_all(void);
void setup_viewport(VkCommandBuffer command_buffer, RenderRect render_area);

// actually implemented in main.c
void delete_model(struct model* m);

#endif
