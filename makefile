ifeq ($(OS),Windows_NT)
	include ./build/windows.mk
else
	include ./build/linux.mk
endif

CFLAGS=-Iinc -Imodels -I$(VULKAN_SDK)/Include -Wall
SHADERS=main.frag.spv main.vert.spv

OBJECTS=main.o vulkan_buffer.o vulkan_image.o vulkan_init.o vulkan_render.o vulkan_shader.o vulkan_util.o platform.o model.o mmu.o renderer.o common.o gamestate.o xorshiftstar.o matrix.o
INCLUDES=inc/common.h inc/cube_data.h inc/gamestate.h inc/listmalloc.h inc/matrix.h inc/platform.h inc/platform_vulkan.h inc/vulkan_wrapper.h models/bat_data.h models/bug_data.h models/building_data.h inc/renderer.h

$(OUT): $(SHADERS) $(OBJECTS) $(INCLUDES)
	gcc $(OBJECTS) $(LFLAGS) -o $@

%.spv: shaders/%
	$(GLS) -V -o $@ $^

platform.o: $(PLATFORM)/$(PLATFORM)_platform.c
	gcc -c $^ $(CFLAGS) -o $@

%.o: src/%.c
	gcc -c $^ $(CFLAGS) -o $@

.PHONY: clean

clean:
	@-$(RM) *.spv
	@-$(RM) *.o
	$(CLEAN)
