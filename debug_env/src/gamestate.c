
#include "gamestate.h"
#include "common.h"
#include "xorshiftstar.h"
#include "mmu.h"
#include "stdlib.h"
#include "stdio.h"

#define KB 1024
#define MB (1024 * KB)
#define GB (1024 * MB)

// TODO: My collision detection is missing literal edge cases -> But I think I covered it.
// TODO: Maybe try some sort of vectorized form for collision detection?

// Handle the case of breaching the acceleration bounds
static void ValidateAcceleration(Physics* physics)
{
    if(physics->accel_x > MAX_ACCELERATION)
    {
        physics->accel_x = MAX_ACCELERATION;
    }
    else if(physics->accel_x < -MAX_ACCELERATION)
    {
        physics->accel_x = -MAX_ACCELERATION;
    }

    if(physics->accel_y > MAX_ACCELERATION)
    {
        physics->accel_y = MAX_ACCELERATION;
    }
    else if(physics->accel_y < -MAX_ACCELERATION)
    {
        physics->accel_y = -MAX_ACCELERATION;
    }

}

// Handle the case of breaching the acceleration bounds
static void ValidateVelocity(Physics* physics)
{
    if(physics->vel_x > MAX_VELOCITY)
    {
        physics->vel_x = MAX_VELOCITY;
    }
    else if(physics->vel_x < -MAX_VELOCITY)
    {
        physics->vel_x = -MAX_VELOCITY;
    }

    if(physics->vel_y > MAX_VELOCITY)
    {
        physics->vel_y = MAX_VELOCITY;
    }
    else if(physics->vel_y < -MAX_VELOCITY)
    {
        physics->vel_y = -MAX_VELOCITY;
    }

}

static void UpdateVelocity(Physics* physics)
{
    //printf("accel -> %f\n", physics->accel_x);
    physics->vel_x = physics->vel_x + (physics->accel_x * (1 / (float) FRAMES_PER_SECOND));
    physics->vel_y = physics->vel_y + (physics->accel_y * (1 / (float) FRAMES_PER_SECOND));

    ValidateVelocity(physics);
}

static void ValidatePosition(Entity* entity)
{
    // TODO: Some impulse calculation thingy to bounce entity off, and use angle magic to make realistic -> Need mass
    //if(entity->coords.x_meter - (entity->size.width / 2) < 0)
    if(entity->coords.x_meter - (entity->size.width / 2) < 0)
    {
        entity->coords.x_meter = entity->size.width;
        //entity->physics.accel_x = -entity->physics.accel_x;
        entity->physics.vel_x = -entity->physics.vel_x;
    }
    else if(entity->coords.x_meter + (entity->size.width / 2) > SCREEN_WIDTH_METERS)
    {
        entity->coords.x_meter = SCREEN_WIDTH_METERS - (entity->size.width / 2);
        //entity->physics.accel_x = -entity->physics.accel_x;
        entity->physics.vel_x = -entity->physics.vel_x;
    }

    if(entity->coords.y_meter - (entity->size.height / 2) < 0)
    {
        entity->coords.y_meter = entity->size.height;
        //entity->physics.accel_y = -entity->physics.accel_y;
        entity->physics.vel_y = -entity->physics.vel_y;
    }
    else if(entity->coords.y_meter + (entity->size.height / 2) > SCREEN_HEIGHT_METERS)
    {
        entity->coords.y_meter = SCREEN_HEIGHT_METERS - (entity->size.height / 2);
        //entity->physics.accel_y = -entity->physics.accel_y;
        entity->physics.vel_y = -entity->physics.vel_y;
    }
}

// Ensure the entity is within the bounds of the map and if not then correct it
static void ValidateEntityWithinBounds(Entity* entity)
{
    Rect entity_rect = GetEntityRectangle(entity);

    // NOTE: Point for entity represents the entity's center
    // NOTE: The entity's center is guaranteed to be within the map's boundaries,
    //          so adding or subtracting half the entity's width to the x meter position would put the entity within the x bounds
    //          A similar statement can be said for the other bounds
    if(entity_rect.x_meter < 0)
    {
        entity->coords.x_meter += -entity_rect.x_meter;
        //entity->physics.accel_x = -entity->physics.accel_x;
        //entity->physics.vel_x = -entity->physics.vel_x;
    }
    else if(entity_rect.x_meter > SCREEN_WIDTH_METERS)
    {
        entity->coords.x_meter -= ((entity->coords.x_meter + entity_rect.width) - SCREEN_WIDTH_METERS);
        //entity->physics.accel_x = -entity->physics.accel_x;
        //entity->physics.vel_x = -entity->physics.vel_x;
    }

    //printf("BEFORE entity y coord = %i\n", entity->coords.y_meter);
    if(entity_rect.y_meter < 0)
    {
        entity->coords.y_meter += -entity_rect.y_meter;
        //entity->physics.accel_y = -entity->physics.accel_y;
        //entity->physics.vel_y = -entity->physics.vel_y;
    }
    else if(entity_rect.y_meter > SCREEN_HEIGHT_METERS)
    {
        entity->coords.y_meter -= ((entity->coords.y_meter + entity_rect.height) - SCREEN_HEIGHT_METERS);
        //entity->physics.accel_y = -entity->physics.accel_y;
        //entity->physics.vel_y = -entity->physics.vel_y;
    }
    //printf("AFTER entity y coord = %i\n", entity->coords.y_meter);
}

// Ensure the player is within the bounds and if not then correct it
static void ValidatePlayerWithinBounds(Player* player)
{
    Entity* entity = &player->entity;
    Rect entity_rect = GetEntityRectangle(entity);

    // NOTE: Point for entity represents the entity's center
    // NOTE: The entity's center is guaranteed to be within the map's boundaries,
    //          so adding or subtracting half the entity's width to the x meter position would put the entity within the x bounds
    //          A similar statement can be said for the other bounds
    if(entity_rect.x_meter < 0)
    {
        entity->coords.x_meter += -entity_rect.x_meter;
        //entity->physics.accel_x = -entity->physics.accel_x;
        //entity->physics.vel_x = -entity->physics.vel_x;
    }
    else if(entity_rect.x_meter + (entity_rect.width) > player->boundary)
    {
        entity->coords.x_meter = player->boundary - (entity->size.width / 2);
        //entity->physics.accel_x = -entity->physics.accel_x;
        //entity->physics.vel_x = -entity->physics.vel_x;
    }

    //printf("BEFORE entity y coord = %i\n", entity->coords.y_meter);
    if(entity_rect.y_meter < 0)
    {
        entity->coords.y_meter += -entity_rect.y_meter;
        //entity->physics.accel_y = -entity->physics.accel_y;
        //entity->physics.vel_y = -entity->physics.vel_y;
    }
    else if(entity_rect.y_meter + (entity_rect.height) > SCREEN_HEIGHT_METERS)
    {
        entity->coords.y_meter = SCREEN_HEIGHT_METERS - (entity->size.height / (float) 2);
        //entity->physics.accel_y = -entity->physics.accel_y;
        //entity->physics.vel_y = -entity->physics.vel_y;
    }
    //printf("AFTER entity y coord = %i\n", entity->coords.y_meter);
}

static void MoveEntity(Entity* entity, uint8_t vertical_movement)
{
    float time_per_frame = 1 / (float) FRAMES_PER_SECOND;
    float time_squared = time_per_frame * time_per_frame;

    entity->coords.x_meter = entity->coords.x_meter + (entity->physics.vel_x * time_per_frame) + ( (entity->physics.accel_x * time_squared) / 2);
    if(vertical_movement)
    {
        entity->coords.y_meter = entity->coords.y_meter + (entity->physics.vel_y * time_per_frame) + ( (entity->physics.accel_y * time_squared) / 2);
    }

    ValidateEntityWithinBounds(entity);
}

static void HandlePlayerMovement(Player* player)
{
    if(player->input.keydown[KeyUp])
    {
        player->entity.physics.accel_y = -CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_y++;
    }
    if(player->input.keydown[KeyDown])
    {
        player->entity.physics.accel_y = CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_y--;
    }
    if(player->input.keydown[KeyRight])
    {
        player->entity.physics.accel_x = CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_x--;
    }
    if(player->input.keydown[KeyLeft])
    {
        player->entity.physics.accel_x = -CONSTANT_ACCEL;
    }
    else
    {
        //player->entity.physics.accel_x++;
    }

    if(!player->input.keydown[KeyUp] && !player->input.keydown[KeyDown])
    {
        player->entity.physics.accel_y = 0;
    }
    if(!player->input.keydown[KeyRight] && !player->input.keydown[KeyLeft])
    {
        player->entity.physics.accel_x = 0;
    }

    //ValidateAcceleration( &(player->entity.physics) );
    UpdateVelocity( &(player->entity.physics) );
    MoveEntity( &(player->entity), 1);
    ValidatePlayerWithinBounds( player );
}

static uint8_t LineHasCollidedWithLine(Line* line1, Line* line2, Point* line_collision_point)
{
    float start1_y, end1_y;
    float start1_x, end1_x;
    if(line1->p1.x_meter < line1->p2.x_meter)
    {
        start1_y = line1->p1.y_meter;
        start1_x = line1->p1.x_meter;
        end1_y = line1->p2.y_meter;
        end1_x = line1->p2.x_meter;
    }
    else
    {
        start1_y = line1->p2.y_meter;
        start1_x = line1->p2.x_meter;
        end1_y = line1->p1.y_meter;
        end1_x = line1->p1.x_meter;
    }

    float start2_y, end2_y;
    float start2_x, end2_x;
    if(line2->p1.x_meter < line2->p2.x_meter)
    {
        start2_y = line2->p1.y_meter;
        start2_x = line2->p1.x_meter;
        end2_y = line2->p2.y_meter;
        end2_x = line2->p2.x_meter;
    }
    else
    {
        start2_y = line2->p2.y_meter;
        start2_x = line2->p2.x_meter;
        end2_y = line2->p1.y_meter;
        end2_x = line2->p1.x_meter;
    }

    float top1_y, bottom1_y;
    if(start1_y > end1_y)
    {
       top1_y = end1_y;
       bottom1_y = start1_y;
    }
    else
    {
        top1_y = start1_y;
        bottom1_y = end1_y;
    }
    float top2_y, bottom2_y;
    if(start2_y > end2_y)
    {
       top2_y = end2_y;
       bottom2_y = start2_y;
    }
    else
    {
        top2_y = start2_y;
        bottom2_y = end2_y;
    }

    uint32_t shared_x = 0, shared_y = 0;
    if( line1->p1.y_meter == line1->p2.y_meter && line2->p1.y_meter == line2->p2.y_meter ) // Both horizontal Lines
    {
        //printf("Horizontal lines!\n");
        // Do the lines share an x?
        if( start2_x >= start1_x && start2_x <= end1_x ) // start2_x is within line 1's x's
        {
            shared_x = start2_x;
        }
        else if( end2_x >= start1_x && end2_x <= end1_x ) // end2_x is within line 1's x's
        {
            shared_x = end2_x;
        }
        else { return 0; } // TODO: No collision, right?

        // The horizontal lines share an x, if they share a y, then they intersect
        return (top1_y == top2_y);
    }
    else if( line1->p1.x_meter == line1->p2.x_meter && line2->p1.x_meter == line2->p2.x_meter ) // Both vertical Lines
    {
        //printf("Vertical lines!\n");
        // Do the lines share a y?
        if( top2_y >= top1_y && top2_y <= bottom1_y ) // top2_y is within line 1's y's
        {
            shared_y = start2_y;
        }
        else if( bottom2_y >= top1_y && bottom2_y <= bottom1_y ) // bottom2_y is within line 1's y's
        {
            shared_y = end2_y;
        }
        else { return 0; } // TODO: No collision, right?

        // The vertical lines share a y, if they share an x, then they intersect
        return (start1_x == start2_x);
    }
    else if( line1->p1.y_meter == line1->p2.y_meter &&
             line2->p1.x_meter == line2->p2.x_meter ) // Horizontal line 1, Vertical line 2
    {
        //printf("Line 1 = Horizontal\t Line 2 = Vertical\n");
        // The horizontal line is somewhere within the y's of the vertical line
        if( top1_y >= top2_y && top1_y <= bottom2_y )
        {
            // The vertical line has its only x the same as an x in the horizontal line
            if( start2_x >= start1_x && start2_x <= end1_x )
            {
                return 1;
            }
        }
        return 0; // TODO: No collision, right?
    }
    else if( line1->p1.x_meter == line1->p2.x_meter &&
             line2->p1.y_meter == line2->p2.y_meter ) // Vertical line 1, Horizontal line 2
    {
        //printf("Line 1 = Vertical\t Line 2 = Horizontal\n");
        // The vertical line is somewhere within the x's of the horizontal line
        if( start1_x >= start2_x && start1_x <= end2_x )
        {
            // The horizontal line has its only y the same as an y in the vertical line
            if( top2_y >= top1_y && top2_y <= bottom1_y )
            {
                return 1;
            }
        }
        return 0; // TODO: No collision, right?
    }

    // Is there some y the lines share?
    if( start2_y >= start1_y && start2_y <= end1_y )
    {
        shared_y = start2_y;
    }
    else if( end2_y >= start1_y && end2_y <= end1_y )
    {
        shared_y = end2_y;
    }
    else { return 0; } // TODO: No collision, right?

    // Is there some x the lines share?
    if( start2_x >= start1_x && start2_x <= end1_x )
    {
        shared_x = start2_x;
    }
    else if( end2_x >= start1_x && end2_x <= end1_x )
    {
        shared_x = end2_x;
    }
    else { return 0; } // TODO: No collision, right?

    float slope1 = (line1->p2.y_meter - line1->p1.y_meter) / (float) (line1->p2.x_meter - line1->p1.x_meter);
    float slope2 = (line2->p2.y_meter - line2->p1.y_meter) / (float) (line2->p2.x_meter - line2->p1.x_meter);

    
    // TODO: This is incorrect. I should want to instead have the "y intercept" be the same
    // TODO: That is, I want to basically have 2 equations for my lines with the same "b" -> set equation from here
    shared_x = shared_y / slope1;

    if( shared_y == slope1 * shared_x || shared_y == slope2 * shared_x )
    {
        return 1;
    }

    // TODO: Need more correct values for shared_x and shared_y (probably?)
    line_collision_point->x_meter = shared_x;
    line_collision_point->y_meter = shared_y;

    return 0;
}

// TODO: Ensure this is correct... Also find a better way?
// TODO: Does this cover the case of rect1 being larger than rect2 so that no corner of rect1 touches rect2 but still a collision?
static uint8_t RectHasCollidedWithRect(Rect* rect1, Rect* rect2, Point* rect1_overlap_point_out)
{
    Line rect1_top, rect1_left, rect1_right, rect1_bottom;
    Line rect2_top, rect2_left, rect2_right, rect2_bottom;

    // Rect1 lines
    rect1_top.p1.x_meter = rect1->x_meter; // Top left point ...
    rect1_top.p1.y_meter = rect1->y_meter;
    rect1_top.p2.x_meter = rect1->x_meter + rect1->width; // Top right point ...
    rect1_top.p2.y_meter = rect1->y_meter;

    rect1_left.p1.x_meter = rect1->x_meter; // Top left point ...
    rect1_left.p1.y_meter = rect1->y_meter;
    rect1_left.p2.x_meter = rect1->x_meter; // Bottom left point ...
    rect1_left.p2.y_meter = rect1->y_meter + rect1->height;

    rect1_right.p1.x_meter = rect1->x_meter + rect1->width; // Top right point ...
    rect1_right.p1.y_meter = rect1->y_meter;
    rect1_right.p2.x_meter = rect1->x_meter + rect1->width; // Bottom right point ...
    rect1_right.p2.y_meter = rect1->y_meter + rect1->height;

    rect1_bottom.p1.x_meter = rect1->x_meter; // Bottom left point ...
    rect1_bottom.p1.y_meter = rect1->y_meter + rect1->height;
    rect1_bottom.p2.x_meter = rect1->x_meter + rect1->width; // Bottom right point ...
    rect1_bottom.p2.y_meter = rect1->y_meter + rect1->height;

    // rect2 lines
    rect2_top.p1.x_meter = rect2->x_meter; // Top left point ...
    rect2_top.p1.y_meter = rect2->y_meter;
    rect2_top.p2.x_meter = rect2->x_meter + rect2->width; // Top right point ...
    rect2_top.p2.y_meter = rect2->y_meter;

    rect2_left.p1.x_meter = rect2->x_meter; // Top left point ...
    rect2_left.p1.y_meter = rect2->y_meter;
    rect2_left.p2.x_meter = rect2->x_meter; // Bottom left point ...
    rect2_left.p2.y_meter = rect2->y_meter + rect2->height;

    rect2_right.p1.x_meter = rect2->x_meter + rect2->width; // Top right point ...
    rect2_right.p1.y_meter = rect2->y_meter;
    rect2_right.p2.x_meter = rect2->x_meter + rect2->width; // Bottom right point ...
    rect2_right.p2.y_meter = rect2->y_meter + rect2->height;

    rect2_bottom.p1.x_meter = rect2->x_meter; // Bottom left point ...
    rect2_bottom.p1.y_meter = rect2->y_meter + rect2->height;
    rect2_bottom.p2.x_meter = rect2->x_meter + rect2->width; // Bottom right point ...
    rect2_bottom.p2.y_meter = rect2->y_meter + rect2->height;


    Point collision_point;
    /*NOTE: KEEP THIS, may want, but for now with faster entities behind slower ones, I need more checking
     * if(LineHasCollidedWithLine(&rect1_top, &rect2_right, &collision_point) ||
       LineHasCollidedWithLine(&rect1_bottom, &rect2_right, &collision_point) ||
       LineHasCollidedWithLine(&rect1_top, &rect2_left, &collision_point) || 
       LineHasCollidedWithLine(&rect1_bottom, &rect2_left, &collision_point) ||
       LineHasCollidedWithLine(&rect1_left, &rect2_top, &collision_point) ||
       LineHasCollidedWithLine(&rect1_right, &rect2_top, &collision_point) ||
       LineHasCollidedWithLine(&rect1_left, &rect2_bottom, &collision_point) ||
       LineHasCollidedWithLine(&rect1_right, &rect2_bottom, &collision_point))
    {
        return 1;
    }*/

    uint8_t collision = 0;
    Line* line1;
    Line* line2;
    for(int i = 0; i < 4 && !collision; i++)
    {
        switch(i)
        {
            case 0: line1 = &rect1_top; break;
            case 1: line1 = &rect1_left; break;
            case 2: line1 = &rect1_right; break;
            case 3: line1 = &rect1_bottom; break;
        }
        for(int j = 0; j < 4 && !collision; j++)
        {
            switch(j)
            {
                case 0: line2 = &rect2_top; break;
                case 1: line2 = &rect2_left; break;
                case 2: line2 = &rect2_right; break;
                case 3: line2 = &rect2_bottom; break;
            }

            /*printf("line1_x1 = %f\n", line1->p1.x_meter);
            printf("line1_y = %f\n", line1->p1.y_meter);
            printf("line1_x2 = %f\n", line1->p2.x_meter);
            printf("line1_y = %f\n", line1->p2.y_meter);

            printf("line2_x1 = %f\n", line2->p1.x_meter);
            printf("line2_y = %f\n", line2->p1.y_meter);
            printf("line2_x2 = %f\n", line2->p2.x_meter);
            printf("line2_y = %f\n", line2->p2.y_meter);*/
            if(LineHasCollidedWithLine(line1, line2, &collision_point))
            {
                //printf("Line collision!\ny spot: %f\nx spot: %f\n", collision_point.y_meter, collision_point.x_meter);
                collision = 1;
            }
        }
    }

    return collision;
}

static Line CreateLine(Point* p1, Point* p2)
{
    Line line;

    line.p1.x_meter = p1->x_meter;
    line.p1.y_meter = p1->y_meter;

    line.p2.x_meter = p2->x_meter;
    line.p2.y_meter = p2->y_meter;

    line.slope = (line.p2.y_meter - line.p1.y_meter) / (line.p2.x_meter - line.p1.x_meter);

    line.y_intercept = line.p1.y_meter - (line.slope * line.p1.x_meter);

    return line;
}

static Point PointOnCircleGivenLine(Circle* circle, Line* line, uint8_t y_coord_pos_flag)
{
    Point coords;

    float b_k_difference = line->y_intercept - circle->center.y_meter;

    float a = Exponentiate(line->slope, 2) + 1;
    float b = (2 * line->slope * b_k_difference) - (2 * circle->center.x_meter);
    float c = Exponentiate(b_k_difference, 2) + Exponentiate(circle->center.x_meter, 2) - Exponentiate(circle->radius, 2);

    float sqrt_portion = NthRoot( 2, Exponentiate(b, 2) - (4 * a * c) );
    
    float root_1 = ( -b + sqrt_portion) / (2 * a);
    float root_2 = ( -b - sqrt_portion) / (2 * a);

    float y_coord_given_root_1 = (line->slope * root_1) + line->y_intercept;
    float y_coord_given_root_2 = (line->slope * root_2) + line->y_intercept;
    
    // NOTE: A ternary operator would just be ugly, but there must certaintly still be a better way...
    // If we want the positive y coordinate
    if(y_coord_pos_flag)
    {
        if(y_coord_given_root_1 > 0)
        {
            coords.x_meter = root_1;
            coords.y_meter = y_coord_given_root_1;
        }
        else
        {
            coords.x_meter = root_2;
            coords.y_meter = y_coord_given_root_2;
        }
    }
    else
    {
        if(y_coord_given_root_1 < 0)
        {
            coords.x_meter = root_1;
            coords.y_meter = y_coord_given_root_1;
        }
        else
        {
            coords.x_meter = root_2;
            coords.y_meter = y_coord_given_root_2;
        }
    }
    
    return coords;
}

static Quadrant GetQuadrantAndNearestRectCornerToCircle(Rect* rect, Circle* circle, Point* coords_out)
{
    Quadrant quadrant;

    // NOTE: Coordinate comparison is relative to the circle's center (i.e. x/y plane has origin at circle center)
    // Rectangle is upper right - relative to circle center
    if( rect->x_meter >= circle->center.x_meter && rect->y_meter >= circle->center.y_meter)
    {
        //y_coord_pos_flag = 1;
        quadrant = TOP_RIGHT;

        coords_out->x_meter = rect->x_meter;
        coords_out->y_meter = rect->y_meter + rect->height;
    }
    // Rectangle is upper left - relative to circle center
    else if( rect->x_meter <= circle->center.x_meter && rect->y_meter >= circle->center.y_meter)
    {
        //y_coord_pos_flag = 1;
        quadrant = TOP_LEFT;

        coords_out->x_meter = rect->x_meter + rect->width;
        coords_out->y_meter = rect->y_meter + rect->height;
    }
    // Rectangle is lower right - relative to circle center
    else if( rect->x_meter >= circle->center.x_meter && rect->y_meter <= circle->center.y_meter)
    {
        //y_coord_pos_flag = 0;
        quadrant = BOTTOM_RIGHT;

        coords_out->x_meter = rect->x_meter;
        coords_out->y_meter = rect->y_meter;
    }
    // Top right of rectangle collided with bottom left of circle
    // Rectangle is lower left - relative to circle center
    else if( rect->x_meter <= circle->center.x_meter && rect->y_meter <= circle->center.y_meter)
    {
        //y_coord_pos_flag = 0;
        quadrant = BOTTOM_LEFT;

        coords_out->x_meter = rect->x_meter + rect->width;
        coords_out->y_meter = rect->y_meter;
    }
    else
    {
        quadrant = INVALID;
    }
    
    return quadrant;
}

static inline float CalculateDistanceBetweenPoints(Point* p1, Point* p2)
{
    return NthRoot(2, Exponentiate(p2->x_meter - p1->x_meter, 2) + Exponentiate(p2->y_meter - p1->y_meter, 2) );
}

static inline uint8_t CircleCollidedWithCircle(Circle* circle1, Circle* circle2)
{
    return (circle1->radius + circle2->radius >= CalculateDistanceBetweenPoints(&circle1->center, &circle2->center));
}

// See if the rectangle has a corner within the circle, do this by drawing a line from the cirlce's center to that nearest corner
//      if this distance is less than the radius, then the rect corner is within the circle.
static inline uint8_t RectCornerWithinCircle(Rect* rect, Circle* circle)
{
    Point p1;
    Point p2 = {.x_meter = circle->center.x_meter, .y_meter = circle->center.y_meter};
    Quadrant quadrant = GetQuadrantAndNearestRectCornerToCircle(rect, circle, &p1);

    float dist_from_circle_center_to_rect_corner = CalculateDistanceBetweenPoints(&p1, &p2);

    if( dist_from_circle_center_to_rect_corner < circle->radius )
    {
        return 1;
    }
    
    return 0;
}

// TODO: Ensure this is correct... Also, find a better way?
static uint8_t RectHasCollidedWithCircle(Rect* rect, Circle* circle)
{
    Point p1;
    Point p2 = {.x_meter = circle->center.x_meter, .y_meter = circle->center.y_meter};

    //uint8_t y_coord_pos_flag;
    Quadrant quadrant = GetQuadrantAndNearestRectCornerToCircle(rect, circle, &p1);

    Line line_through_rect_and_circle_center = CreateLine(&p1, &p2);
    Point point_on_circle_and_line = PointOnCircleGivenLine(circle, &line_through_rect_and_circle_center, quadrant > 0);

    // NOTE: Because the quadrant is relative to the circle's center and because 
    //          we know what the rectangle's corresponding corner (and what that corner's quadrant is), 
    //          we only need to test the point on the cirlce and the point on the rectangle's corner
    // NOTE: Point here are relative to an origin in the top left corner of screen (like normal)
    //          But quadrant is relative to circle's center
    switch(quadrant)
    {
        case TOP_RIGHT:
        {
            if( (rect->x_meter <= point_on_circle_and_line.x_meter &&
                 rect->y_meter >= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
        case TOP_LEFT:
        {
            if( (rect->x_meter >= point_on_circle_and_line.x_meter &&
                 rect->y_meter >= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
        case BOTTOM_LEFT:
        {
            if( (rect->x_meter >= point_on_circle_and_line.x_meter &&
                 rect->y_meter <= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
        case BOTTOM_RIGHT:
        {
            if( (rect->x_meter <= point_on_circle_and_line.x_meter &&
                 rect->y_meter <= point_on_circle_and_line.y_meter) )
            {
                return 1;
            }
        } break;
    }

    return 0;
}

    
static inline void ValidateNoEntityOverlap(Entity* entity, Entity* other_entities, uint32_t num_entities)
{
    Rect entity_rect = GetEntityRectangle( entity );
    // Ensure no bugs overlap
    for(int j = 0; j < num_entities; j++)
    {
        Rect other_entity_rect = GetEntityRectangle( &other_entities[j]);
        Point rect1_overlap_point;
        float x_diff = 0, y_diff = 0;
        switch(RectHasCollidedWithRect(&entity_rect, &other_entity_rect, &rect1_overlap_point))
        {
            // TODO: Maybe this is the same as "Bottom_right into top left"? -> May be able to simplify
            // TODO: There is a lot of repeated code here with the adjustment conditionals -> Find way to simplify
            // We are wanting to find the difference in meters betwen the collision corner of rect1 and rect2's edge
            case TOP_LEFT_INTO_BOTTOM_RIGHT:
            {
                x_diff = (other_entity_rect.x_meter + other_entity_rect.width) - rect1_overlap_point.x_meter;
                y_diff = (other_entity_rect.y_meter + other_entity_rect.height) - rect1_overlap_point.y_meter;

                // Attempt to move rect1 to the right the overlapped amount
                if( (entity_rect.x_meter + entity_rect.width) - x_diff < SCREEN_WIDTH_METERS )
                {
                    entity->coords.x_meter -= x_diff;
                    //entity->physics.accel_x = CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_x = -entity->physics.accel_x;
                }
                // Else, move rect2 to the left the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter -= x_diff;
                    //entity->physics.accel_x = CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_x = -CONSTANT_ACCEL;
                }

                // TODO: This shouldn't be needed, as long as we can move the rect in some direction out of rectangle
                // Attempt to move rect1 down the overlapped amount
                if( (entity_rect.y_meter + entity_rect.height) + y_diff < SCREEN_HEIGHT_METERS )
                {
                    entity->coords.y_meter += y_diff;
                    //entity->physics.accel_y = CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                }
                // Else, move rect2 up the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter -= y_diff;
                    //entity->physics.accel_y = CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                }
            } break;

            case TOP_RIGHT_INTO_BOTTOM_LEFT:
            {
                x_diff = rect1_overlap_point.x_meter - (other_entity_rect.x_meter);
                y_diff = (other_entity_rect.y_meter + other_entity_rect.height) - rect1_overlap_point.y_meter;

                // Attempt to move rect1 to the left the overlapped amount
                if( (entity_rect.x_meter) - x_diff > 0 )
                {
                    entity->coords.x_meter -= x_diff;
                    //entity->physics.accel_x = -CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                }
                // Else, move rect2 to the right the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter += x_diff;
                    //other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                    //entity->physics.accel_x = -CONSTANT_ACCEL;
                }

                // Attempt to move rect1 down the overlapped amount
                if( (entity_rect.y_meter + entity_rect.height) + y_diff < SCREEN_HEIGHT_METERS )
                {
                    entity->coords.y_meter += y_diff;
                    //entity->physics.accel_y = CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                }
                // Else, move rect2 up the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter -= y_diff;
                    //other_entities[j].physics.accel_y = -CONSTANT_ACCEL;
                    //entity->physics.accel_y = CONSTANT_ACCEL;
                }
            } break;

            case BOTTOM_LEFT_INTO_TOP_RIGHT:
            {
                x_diff = (other_entity_rect.x_meter + other_entity_rect.width) - rect1_overlap_point.x_meter;
                y_diff = rect1_overlap_point.y_meter - (other_entity_rect.y_meter);

                // Attempt to move rect1 to the right the overlapped amount
                if( (entity_rect.x_meter + entity_rect.width) - x_diff < SCREEN_WIDTH_METERS )
                {
                    entity->coords.x_meter += x_diff;
                    //entity->physics.accel_x = CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                }
                // Else, move rect2 to the left the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter -= x_diff;
                    //other_entities[j].physics.accel_x = -CONSTANT_ACCEL;
                    //entity->physics.accel_x = CONSTANT_ACCEL;
                }

                // Attempt to move rect1 up the overlapped amount
                if( (entity_rect.y_meter) - y_diff > 0 )
                {
                    entity->coords.y_meter -= y_diff;
                    //entity->physics.accel_y = -CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                }
                // Else, move rect2 down the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter += y_diff;
                    //other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                    //entity->physics.accel_y = -CONSTANT_ACCEL;
                }
            } break;

            case BOTTOM_RIGHT_INTO_TOP_LEFT:
            {
                x_diff = rect1_overlap_point.x_meter - (other_entity_rect.x_meter);
                y_diff = rect1_overlap_point.y_meter - (other_entity_rect.y_meter);

                // Attempt to move rect1 to the left the overlapped amount
                if( (entity_rect.x_meter) - x_diff > 0 )
                {
                    entity->coords.x_meter -= x_diff;
                    //entity->physics.accel_x = -CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_x = -CONSTANT_ACCEL;
                }
                // Else, move rect2 to the right the overlapped amount
                else
                {
                    other_entities[j].coords.x_meter += x_diff;
                    //other_entities[j].physics.accel_x = CONSTANT_ACCEL;
                    //entity->physics.accel_x = -CONSTANT_ACCEL;
                }

                // Attempt to move rect1 up the overlapped amount
                if( (entity_rect.y_meter) - y_diff > 0 )
                {
                    entity->coords.y_meter -= y_diff;
                    //entity->physics.accel_y = -CONSTANT_ACCEL;
                    //other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                }
                // Else, move rect2 down the overlapped amount
                else
                {
                    other_entities[j].coords.y_meter += y_diff;
                    //other_entities[j].physics.accel_y = CONSTANT_ACCEL;
                    //entity->physics.accel_y = -CONSTANT_ACCEL;
                }
            } break;
        }
    }
}

static inline void DestroyNPC(struct NPC** npc, uint32_t* num_npcs, uint32_t* npc_numbers)
{
    switch( (*npc)->type )
    {
        case HAWK:
        {
            npc_numbers[HAWK]--;
        } break;

        case BUILDING:
        {
            npc_numbers[BUILDING]--;
        } break;

        case BUG:
        {
            npc_numbers[BUG]--;
        } break;
    }
    (*num_npcs)--;

    //printf("npc = %d\n", npc);
    //printf("npc* = %d\n", *npc);
    mmu_free(*npc);
    *npc = NULL;
}


static void HandleCollisionDetection(GameState* state)
{
    Rect player_rect = GetEntityRectangle(&(state->player.entity));

    for(int i = 0; i < state->num_npcs; i++)
    {
        Rect npc_rect = GetEntityRectangle(&(state->npcs[i]->entity));

        if(RectHasCollidedWithRect(&player_rect, &npc_rect, 0))
        {
            if(state->npcs[i]->type == BUG)
            {
                DestroyNPC(&(state->npcs[i]), &(state->num_npcs), state->npc_numbers);
                state->npcs[i] = state->npcs[state->num_npcs];
                state->npcs[state->num_npcs] = NULL;

                state->player.stamina += BUG_STAMINA_BOOST;
            }
            else
            {
                state->game_over = 1;
            }
        }
    }
}

// TODO: Currently needing to have a spawn frame of buildings separate from a spawn frame of bugs
static void CreateNPC(Difficulty difficulty, uint64_t logical_frames, struct NPC** npcs, uint32_t* num_npcs, uint32_t* npc_numbers, Player* player)
{
    int npc_index = (*num_npcs)++;
    //printf("npc_index = %i\n", npc_index);
    //printf("num_npcs = %i\n", *num_npcs);

    if(xorshift64star_uniform(6) % 3 == 0)
    {
        if(xorshift64star_uniform(6) % 2 == 0)
        {
            npcs[npc_index] = mmu_alloc(BUILDING);
            npcs[npc_index]->type = BUILDING;
            npc_numbers[BUILDING]++;

            npcs[npc_index]->entity.size.width = xorshift64star_float(MAX_BUILDING_WIDTH - MAX_BUILDING_WIDTH );
            npcs[npc_index]->entity.size.width += MIN_BUILDING_WIDTH;
            npcs[npc_index]->entity.size.height = xorshift64star_float(MAX_BUILDING_HEIGHT - MIN_BUILDING_HEIGHT);
            npcs[npc_index]->entity.size.height += MIN_BUILDING_HEIGHT;
        }
        else
        {
            npcs[npc_index] = mmu_alloc(HAWK);
            npcs[npc_index]->type = HAWK;
            npc_numbers[HAWK]++;

            npcs[npc_index]->entity.size.width = xorshift64star_float(MAX_HAWK_WIDTH - MIN_HAWK_WIDTH);
            npcs[npc_index]->entity.size.width += MIN_HAWK_WIDTH;
            npcs[npc_index]->entity.size.height = xorshift64star_float(MAX_HAWK_HEIGHT - MIN_HAWK_HEIGHT);
            npcs[npc_index]->entity.size.height += MIN_HAWK_HEIGHT;
        }
    }
    else
    {
        npcs[npc_index] = mmu_alloc(BUG);
        npcs[npc_index]->type = BUG;
        npc_numbers[BUG]++;

        npcs[npc_index]->entity.size.width = xorshift64star_float(MAX_BUG_WIDTH - MIN_BUG_WIDTH);
        npcs[npc_index]->entity.size.width += MIN_BUG_WIDTH;
        npcs[npc_index]->entity.size.height = xorshift64star_float(MAX_BUG_HEIGHT - MIN_BUG_HEIGHT);
        npcs[npc_index]->entity.size.height += MIN_BUG_HEIGHT;
    }

    npcs[npc_index]->entity.health = 0;

    // npcs always move from right to left (sliding effect)
    npcs[npc_index]->entity.physics.accel_x = - CONSTANT_ACCEL * difficulty;
    // TODO: Get this working. I want entities faster as time goes on
    if(logical_frames % 500 < 5)
    {
        npcs[npc_index]->entity.physics.accel_x -= (logical_frames / 1000) * (0.05 * difficulty);
        printf("Accelerating entities!\nAccel = %f\n", npcs[npc_index]->entity.physics.accel_x);
    }
    npcs[npc_index]->entity.physics.accel_y = 0;
    npcs[npc_index]->entity.physics.vel_x = 0;
    npcs[npc_index]->entity.physics.vel_y = 0;

    npcs[npc_index]->entity.coords.x_meter = SCREEN_WIDTH_METERS;
    npcs[npc_index]->entity.coords.x_meter -= (npcs[npc_index]->entity.size.width);

    uint8_t unique_position = 0;
    while(!unique_position)
    {
        unique_position = 1;

        npcs[npc_index]->entity.coords.y_meter = xorshift64star_uniform(SCREEN_HEIGHT_METERS);
        npcs[npc_index]->entity.coords.y_meter -= (npcs[npc_index]->entity.size.height / 2);

        // This will ensure the entity is within the boundaires (so IDC about entity being OOB above)
        ValidateEntityWithinBounds( &(npcs[npc_index]->entity) );

        Rect new_npc_rect = GetEntityRectangle( &(npcs[npc_index]->entity) );

        for(int i = 0; i < (*num_npcs) - 1; i++)
        { 
            Rect other_npc_rect = GetEntityRectangle( &(npcs[i]->entity) );
            if(RectHasCollidedWithRect( &new_npc_rect, &other_npc_rect, 0 ))
            {
                unique_position = 0;
                break;
            }
        }
    }
}

static inline void CopyNPC(struct NPC* source, struct NPC* dest)
{
    dest->type = source->type;
    dest->entity = source->entity;
}

// TODO: Create state variable for last creation frame of Bug, then if say 3 * FPS passed, then create Bug
//          Then update that variable to the new frame.
//          Do I want this? It adds a whole 'nother 64 bit variable and doesn't add that much more value. 
//               It's meaning can be derived with simple modulo arithmetic
static void HandleNPCUpdates(GameState* state)
{
    double seconds_passed = (state->logical_frames / (double) FRAMES_PER_SECOND);

    // TODO: Figure this spawning frame thing out... Preferably without needing to create another variable in GameState
    // Spawn a new Bug every so many frames depending on difficulty and SPAWN_RATE (+1 to compensate for any div0)
    uint32_t spawn_frame = (FRAMES_PER_SECOND * 8) * SPAWN_RATE;
    spawn_frame += SPAWN_FRAME_BASE;
    spawn_frame -= (30) * state->difficulty;
    spawn_frame -= seconds_passed;
    if(spawn_frame < SPAWN_FRAME_MIN) {
        spawn_frame = SPAWN_FRAME_MIN;
    }
    uint32_t building_spawn_frame = spawn_frame * (float) (3/2);
    //printf("Difficulty = %i\n", state->difficulty);
    //printf("Spawn rate = %f\n", SPAWN_RATE);
    //printf("Spawn frame = %i\n", spawn_frame);
    if( ( state->logical_frames % spawn_frame ) == 0 )
    {
        //printf("Time to create an Bug! Spawn frame every %i frames\n", spawn_frame);
        printf("There has been %i frames\n", state->logical_frames);
        //printf("This corresponds to %f seconds\n", state->logical_frames / (float) FRAMES_PER_SECOND);

        CreateNPC(state->difficulty, state->logical_frames, (state->npcs), &(state->num_npcs), state->npc_numbers, &(state->player));
        if( state ->logical_frames % (spawn_frame * 3) == 0 ) 
        {
            printf("TRIPPLE SPAWN!\n");
            CreateNPC(state->difficulty, state->logical_frames, (state->npcs), &(state->num_npcs), state->npc_numbers, &(state->player));
            CreateNPC(state->difficulty, state->logical_frames, (state->npcs), &(state->num_npcs), state->npc_numbers, &(state->player));
        }
    }

    for(int i = 0; i < state->num_npcs; i++)
    {
        if(state->npcs[i]->entity.coords.x_meter - (state->npcs[i]->entity.size.width / 2) <= 0)
        {
            DestroyNPC(&(state->npcs[i]), &(state->num_npcs), state->npc_numbers);
            state->npcs[i] = state->npcs[state->num_npcs];
            state->npcs[state->num_npcs] = NULL;
            
            continue;
        }

        // NOTE: Commented out because no entity should overlap
        /*uint32_t num_other_entities = state->num_npcs - 1;
        uint32_t entity_index = 0;
        Entity* other_entities = malloc(sizeof(Entity) * num_other_entities);
        for(int j = 0; j < state->num_npcs; j++)
        {
            if(i != j)
            {
                other_entities[entity_index++] = state->npcs[j].entity;
            }
        }

        ValidateNoEntityOverlap( &state->npcs[i].entity, other_entities, num_other_entities );*/

        UpdateVelocity( &(state->npcs[i]->entity.physics) );
        MoveEntity( &(state->npcs[i]->entity), 0 );
    }
}

// Entities are defined with position referring to the center, we want a rectangle defined with x/y being top left
Rect GetEntityRectangle(Entity* entity)
{
    Rect rect;

    rect.width = entity->size.width;
    rect.height = entity->size.height;

    // Top left position of rectangle
    rect.x_meter = entity->coords.x_meter - (rect.width / 2);
    rect.y_meter = entity->coords.y_meter - (rect.height / 2);

    return rect;
}

void InitializeGameState(GameState* state)
{
    state->game_over = 0;
    state->running = 1;
    state->paused = 0;
    state->logical_frames = 0;
    state->fps = FRAMES_PER_SECOND;

    // TODO: Player determined?
    // TODO: Demo is broken.. bad (New bugs accelerate, but none should, so need to ensure the difficulty accel is in update of Bug accel)
    //state->difficulty = Demo; 
    state->difficulty = Easy;
    //state->difficulty = Medium;
    //state->difficulty = Hard;
    //state->difficulty = Impossible;

    state->player.stamina = INITIAL_STAMINA;
    state->player.entity.health = 0;

    state->player.entity.size.width = xorshift64star_float(MAX_PLAYER_WIDTH - MIN_PLAYER_WIDTH);
    state->player.entity.size.width += MIN_PLAYER_WIDTH;
    state->player.entity.size.height = xorshift64star_float(MAX_PLAYER_HEIGHT - MIN_PLAYER_HEIGHT);
    state->player.entity.size.height += MIN_PLAYER_HEIGHT;

    // Start player in top left corner
    state->player.entity.coords.x_meter = 1 + (state->player.entity.size.width / 2);
    state->player.entity.coords.y_meter = (SCREEN_HEIGHT_METERS / 2) + (state->player.entity.size.height / 2);
    //printf("Start x = %f\nStart y = %f\n", state->player.entity.coords.x_meter, state->player.entity.coords.y_meter);

    state->player.entity.physics.accel_x = 0;
    state->player.entity.physics.accel_y = 0;
    state->player.entity.physics.vel_x = 0;
    state->player.entity.physics.vel_y = 0;

    switch(state->difficulty)
    {
        case Demo:
        {
            state->player.boundary = BOUNDARY_DEMO;
        } break;

        case Easy:
        {
            state->player.boundary = BOUNDARY_EASY;
        } break;

        case Medium:
        {
            state->player.boundary = BOUNDARY_MEDIUM;
        } break;

        case Hard:
        {
            state->player.boundary = BOUNDARY_HARD;
        } break;

        case Impossible:
        {
            state->player.boundary = BOUNDARY_IMPOSSIBLE;
        } break;
    }

    for(int i = 0; i < KeyNum; i++)
    {
        state->player.input.keydown[i] = 0;
    }

    state->num_bugs = 0;
    state->num_hawks = 0;
    state->num_buildings = 0;

    //state->npcs = NULL;
    state->npc_numbers[HAWK] = state->num_hawks;
    state->npc_numbers[BUILDING] = state->num_buildings;
    state->npc_numbers[BUG] = state->num_bugs;
    state->num_npcs = state->npc_numbers[HAWK] + state->npc_numbers[BUILDING] + state->npc_numbers[BUG];

    for(int i = 0; i < INITIAL_NPC_COUNT; i++)
    {
        CreateNPC(state->difficulty, 0, (state->npcs), &(state->num_npcs), state->npc_numbers, &(state->player));
    }
}

void UpdateGameState(GameState* state)
{
    state->logical_frames++;
    HandlePlayerMovement(&(state->player));
    HandleNPCUpdates(state);
    HandleCollisionDetection(state);

    // TODO: End game thingy, restart? prompt? close? show score?
    if(state->game_over && state->difficulty)
    {
        printf("GameOver!\n");
        //state->running = 0; // Current just ending the entire game upon death
    }

}

void DestroyGameState(GameState* state)
{
    for(int i =0; i < state->num_npcs; i++)
    {
        DestroyNPC(&(state->npcs[i]), &(state->num_npcs), state->npc_numbers);
    }
}



/*int main()
{
    GameState state;

    InitializeGameState(&state);
    printf("Gamestate Initialized!\n");

    //while(state.running)
    {
        UpdateGameState(&state);
    }

    DestroyGameState(&state);
    printf("Gamestate Destroyed!\n");
}*/




