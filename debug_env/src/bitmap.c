

#include "bitmap.h"
#include "common.h"


void InitializeBitmap(Bitmap* bitmap, int width, int height)
{
    if( bitmap->pixels )
        VirtualFree( bitmap->pixels, 0, MEM_RELEASE );

    bitmap->width = width;
    bitmap->height = height;

    bitmap->bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bitmap->bmi.bmiHeader.biWidth = bitmap->width;
    bitmap->bmi.bmiHeader.biHeight = -bitmap->height; // Negative so bitmap->will be displayed top down (0,0 is top left of screen)
    bitmap->bmi.bmiHeader.biPlanes = 1;
    bitmap->bmi.bmiHeader.biBitCount = 32;
    bitmap->bmi.bmiHeader.biCompression = BI_RGB;
    bitmap->bmi.bmiHeader.biSizeImage = 0;
    bitmap->bmi.bmiHeader.biXPelsPerMeter = 0;
    bitmap->bmi.bmiHeader.biYPelsPerMeter = 0;
    bitmap->bmi.bmiHeader.biClrUsed = 0;
    bitmap->bmi.bmiHeader.biClrImportant = 0;

    bitmap->bytes_per_pixel = bitmap->bmi.bmiHeader.biBitCount / 8;

    // TODO: OTHER STUFF???
    
    uint32_t bitmap_size = (width * height) * (bitmap->bytes_per_pixel); // NumPixels * BytesPerPixel
    bitmap->pixels = VirtualAlloc(0, bitmap_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

    ValidateObject(bitmap->pixels, "bitmap->>pixels");
}




