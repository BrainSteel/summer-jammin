

#include "windows.h"

#include "common.h"
#include "gamestate.h"
#include "bitmap.h"
#include "pixel.h"

#include "stdio.h"
#include "stdint.h"

Bitmap bitmap;
GameState state;


void HandlePaintEvent(HWND hwnd, HDC hdc);

// hwnd: Handle to the window
// uMsg: The message code (e.g. WM_SIZE message which indicates the window was resized)
// wParam and lParam: Additional data pertaining to the message -> exact meaning depends on message code (uMsg)
// LRESULT is an integer value that your program returns to Windows, it contains the programs'r esponse to the message
//      The exact meaning of this return value depends on the message code (uMsg)
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        // REGION: Window
        case WM_CREATE:
        {

        } break;

        case WM_SIZE:
        {
            RECT rect;
            GetClientRect( hwnd, &rect );
            uint32_t width = rect.right - rect.left;
            uint32_t height = rect.bottom - rect.top;

            InitializeBitmap(&bitmap, width, height);
            state.m_to_px_conversion_factor_x = width / SCREEN_WIDTH_METERS;
            state.m_to_px_conversion_factor_y = height / SCREEN_HEIGHT_METERS;
        } break;

        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* min_max_info = (MINMAXINFO*) lParam;

            min_max_info->ptMinTrackSize.x = MIN_SCREEN_WIDTH_PX;
            min_max_info->ptMinTrackSize.y = MIN_SCREEN_HEIGHT_PX;
        } break;

        // Triggered by hitting close button (red X) or ALT-F4, and any other closing actions
        case WM_CLOSE:
        {
            state.running = 0;
            DestroyWindow(hwnd); // Destroy the specified window 
            PostQuitMessage(0); // Send a quit message to have GetMessage return 0 (thus stop receiving new messages)
        } break;

        // Triggered after DestroyWindow (called by defalt for WM_CLOSE) and after window removed but before destruction
        case WM_DESTROY:
        {

        }

        // REGION: Paint
        // The message sent by OS when we must repaint the window
        // NOTE: OS handles painting of title bar and surrounding frame. The "Client Area" is our only responsibility
        // Only the "Update Region" (not whole client region) gets painted, this is the region that has had something change
        // Every time we paint to the update region, we clear the update region so that OS knows we are done with that area
        // E.g. if a user moves a window over our client area, then the update region is under that moved window
        //      The update region does not need to be redrawn since a window is now obstructing its view
        //      A similar situation happens when stretching the window
        // During the paint event you can either paint the entire client region or just the update region (your choice)
        //      Code is simpler if you just paint everything, but more efficient if you only paint update region
        //      The rcPaint member of PAINTSTRUCT will contain the region that needs updated
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            // All painting occurs between BeginPaint and EndPaint
            HDC hdc = BeginPaint(hwnd, &ps); // Begin painting region

            if( !hdc ) { printf("BeginPaint failed!\n"); }

            HandlePaintEvent(hwnd, hdc);

            EndPaint(hwnd, &ps); // End painting region -> Clears the update region
        } break;

        // REGION: INPUT
        case WM_LBUTTONDOWN:
        {

        } break;

        case WM_LBUTTONUP:
        {

        } break;

        case WM_RBUTTONDOWN:
        {

        } break;

        case WM_RBUTTONUP:
        {
            
        } break;

        case WM_KEYDOWN:
        {
            switch(wParam)
            {
                case 0x57: // W Key -> Up
                case VK_UP: // Up arrow key
                {
                    state.player.input.keydown[KeyUp] = 1;
                } break;

                case 0x41: // A Key -> Left
                case VK_LEFT: // Left arrow key
                {
                    state.player.input.keydown[KeyLeft] = 1;
                } break;

                case 0x53: // S Key -> Down
                case VK_DOWN: // Down arrow key
                {
                    state.player.input.keydown[KeyDown] = 1;
                } break;

                case 0x44: // D Key -> Right
                case VK_RIGHT: // Right arrow key
                {
                    state.player.input.keydown[KeyRight] = 1;
                } break;

                case VK_ESCAPE:
                {
                    state.paused = state.paused ? 0 : 1;
                } break;

                case 0x58: // X Key -> Exit if Paused
                {
                    if(state.paused) { state.running = 0; }
                } break;
            }
        } break;

        case WM_KEYUP:
        {
            switch(wParam)
            {
                case 0x57: // W Key -> Up
                case VK_UP: // Up arrow key
                {
                    state.player.input.keydown[KeyUp] = 0;
                } break;

                case 0x41: // A Key -> Left
                case VK_LEFT: // Left arrow key
                {
                    state.player.input.keydown[KeyLeft] = 0;
                } break;

                case 0x53: // S Key -> Down
                case VK_DOWN: // Down arrow key
                {
                    state.player.input.keydown[KeyDown] = 0;
                } break;

                case 0x44: // D Key -> Right
                case VK_RIGHT: // Right arrow key
                {
                    state.player.input.keydown[KeyRight] = 0;
                } break;
            }
        } break;

        default:
        {
            return DefWindowProc(hwnd, uMsg, wParam, lParam); // Handle the message in its default manner
        } break;
    }
}

void DrawGameState()
{
    uint32_t player_loc_x_px = state.m_to_px_conversion_factor_x * state.player.entity.coords.x_meter;
    uint32_t player_loc_y_px = state.m_to_px_conversion_factor_y * state.player.entity.coords.y_meter;
    //printf("player x coords = %f\nplayer y coords = %f\n", state.player.entity.coords.x_meter, state.player.entity.coords.y_meter);
    //printf("player_loc_x_px = %i\nplayer_loc_y_px = %i\n", player_loc_x_px, player_loc_y_px);

    uint32_t player_width_px = state.m_to_px_conversion_factor_x * state.player.entity.size.width;
    uint32_t player_height_px = state.m_to_px_conversion_factor_y * state.player.entity.size.height;
    //printf("player_width_px = %i\nplayer_height_px = %i\n", player_width_px, player_height_px);

    RGB color = {.r=255, .g=0, .b=0, .a=0};

    Rect_px rect;
    rect.left = player_loc_x_px - (player_width_px / 2);
    rect.top = player_loc_y_px - (player_height_px / 2);
    rect.right = player_loc_x_px + (player_width_px / 2);
    rect.bottom = player_loc_y_px + (player_height_px / 2);
    //printf("Player top left = (%i,%i)\n", rect.left, rect.top);
    DrawRectangle(&bitmap, &rect, &color);

    // If you want to draw the proximity circle that is around the player
    if( 0 )
    {
        RGB boundary_line_color = {.r=0, .g=0, .b=255, .a=0};
        Line_px boundary_line = {.p1.x = state.player.boundary, .p1.y = 0, 
                                 .p2.x = state.player.boundary, .p2.y = bitmap.height};
        DrawLine(&bitmap, &boundary_line, &boundary_line_color);
    }

    for(int i = 0; i < state.num_npcs; i++)
    {
        uint32_t bug_loc_x_px = state.m_to_px_conversion_factor_x * state.npcs[i]->entity.coords.x_meter;
        uint32_t bug_loc_y_px = state.m_to_px_conversion_factor_y * state.npcs[i]->entity.coords.y_meter;
        //printf("bug_loc_x_px = %i\nbug_loc_y_px = %i\n", bug_loc_x_px, bug_loc_y_px);

        uint32_t bug_width_px = state.m_to_px_conversion_factor_x * state.npcs[i]->entity.size.width;
        uint32_t bug_height_px = state.m_to_px_conversion_factor_y * state.npcs[i]->entity.size.height;

        RGB color;
        if(state.npcs[i]->type == BUG)
        {
            color.r=0;
            color.g=0;
            color.b=255;
            color.a=0;
        }
        else
        {
            color.r=0;
            color.g=255;
            color.b=0;
            color.a=0;
        }

        Rect_px rect;
        rect.left = bug_loc_x_px - (bug_width_px / 2);
        rect.top = bug_loc_y_px  - (bug_height_px / 2);
        rect.right = bug_loc_x_px  + (bug_width_px / 2);
        rect.bottom = bug_loc_y_px + (bug_height_px / 2);
        DrawRectangle(&bitmap, &rect, &color);
    }

    if(state.paused)
    {
        RGB pause_color = {.r=255, .g=255, .b=255, .a=0};
        for(int i = 0; i < 2; i++)
        {
            Rect_px rect;
            rect.left = bitmap.width - 3 * ( 10 * (i+1) );
            rect.top = 10;
            rect.right = rect.left + 10;
            rect.bottom = rect.top + 40;
            DrawRectangle(&bitmap, &rect, &pause_color);
        }
    }

    if(state.game_over)
    {
        RGB end_game_color = {.r=255, .g=255, .b=255, .a=0};
        Rect_px end_game_rect;
        end_game_rect.left = (bitmap.width / 2.0) - 50;
        end_game_rect.right = end_game_rect.left + 100;
        end_game_rect.top = (bitmap.height / 2.0) - 50;
        end_game_rect.bottom = end_game_rect.top + 100;
        //FillRectangle(&bitmap, &end_game_rect, &end_game_color);
        
        RGB repeat_symbol_color = {.r=255, .g=0, .b=0, .a=0};
        Circle_px repeat_symbol;
        repeat_symbol.radius = ((end_game_rect.right - end_game_rect.left) / (float) 2) - 20;
        repeat_symbol.center.x = ((end_game_rect.right + end_game_rect.left) / (float) 2);
        repeat_symbol.center.y = end_game_rect.bottom - repeat_symbol.radius - 10;
        DrawRepeatSymbol(&bitmap, &repeat_symbol, 10, &repeat_symbol_color);
    }
}

void HandlePaintEvent(HWND hwnd, HDC hdc2)
{
    if(!hdc2) { printf("Failed to retrieve handle to device context!\n"); }

}


int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    // Register Window Class
    const char* CLASS_NAME = "My Window Class";

    WNDCLASS wc = { };

    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    // Create an instance of the window class
    HWND hwnd = CreateWindowEx(
            0, // Default window styles
            CLASS_NAME, // Window class
            "My window text!", // Text to go on the window title bar
            WS_OVERLAPPEDWINDOW, // Window style (several flags combined... include title bar, border, sytem menu, min/max,...

            // Size and position
            CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, // Let Windows choose

            NULL, // Parent window (there is none, i.e. this is a top level window)
            NULL, // Menu (there is none)
            hInstance, // Instance handle for our window
            NULL // Additional application data (there is none)
        );

    if(!hwnd)
    {
        printf("Window failed to be created!");
    }

    // Show the window
    ShowWindow(hwnd, nCmdShow); // nCmdShow can be used to either minimize or maximize the window (passed by OS)
    
    // Handle messages
    MSG msg;

    InitializeGameState(&state);

    // Loop until WM_QUIT message is received (can quit program manually with PostQuitMessage(0);)
    // Once GetMessage receives WM_QUIT it doesn't even pass it to Window Procedure to handle it, it just returns 0
    // DispatchMessage is what passed the message to the window's Window Procedure, so it doesn't even have a chance
    // A message sent by the OS can skip the queue, this is called "Sending a message" (goes directly to WindProc)
    // Whereas the event where a message gets put in the queue is called "Posting a message"
    //while( GetMessage(&msg, NULL, 0, 0) ) // msg will contain message details, NULL = want messages for all windows
    while( state.running )
    {
        RGB bkgnd = {.r=0, .g=0, .b=0, .a=0};
        ColorPixels(&bitmap, &bkgnd);
        if(!state.paused && !state.game_over) { UpdateGameState(&state); }
        DrawGameState();

    // TODO: Need to clean this unindented block of code up. This was copied here during a debugging frenzy
    HDC hdc = GetDC(hwnd);

    if(!hdc) { printf("Failed to retrieve handle to device context!\n"); }

    RECT client_rect;
    GetClientRect(hwnd, &client_rect);

    int x_dest = 0;
    int y_dest = 0;
    int dest_width = bitmap.width;
    int dest_height = bitmap.height;
    int x_src = 0;
    int y_src = 0;
    int src_width = client_rect.right - client_rect.left; // width of source rectangle
    int src_height = client_rect.bottom - client_rect.top; // height of source rectangle

    StretchDIBits(
        hdc,
        x_dest,
        y_dest,
        dest_width,
        dest_height,
        x_src,
        y_src,
        src_width,
        src_height,
        bitmap.pixels,
        &bitmap.bmi,
        DIB_RGB_COLORS,
        SRCCOPY
    );

    ReleaseDC(hwnd, hdc);


        UpdateWindow(hwnd);




        //GetMessage(&msg, NULL, 0, 0); // msg will contain message details, NULL = want messages for all windows
        // Peek message is used to be non-blocking, if a message is there, it passes it along, but otherwise it will just ignore it.
        if(PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE)) // msg will contain message details, NULL = want messages for all windows.
        {
            TranslateMessage(&msg); // Translates things such as keystrokes into characters
            DispatchMessage(&msg); // Send the message to the Window Procedure for whatever window is associated with that message
        }
    }

    DestroyGameState(&state);



    return 0;
}


