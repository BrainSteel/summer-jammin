#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_OBJ 1000

//#include "mmu.h"
#include "gamestate.h"
struct NPC objects[MAX_OBJ];

bool free_objects[MAX_OBJ] = {};

struct NPC *mmu_alloc(enum obj_type type)
{
  if(type > BUG || type < HAWK) {
    printf("FATAL MMU ERROR: TYPE OUT OF BOUNDS\n");
    exit(-1);
  }

  for(int i = 0;
      i < MAX_OBJ;
      i++) {
    if(free_objects[i] == false){
        //printf("alloc'ing obj #%d\n", i);
      free_objects[i] = true;
      memset(&objects[i], 0, sizeof(struct NPC));
      objects[i].type = type;
      return &objects[i];
    }
  }
//printf("didn't get an obj\n");
return NULL;
}

void mmu_free(struct NPC *o)
{
  if(o == NULL){
    //printf("mmu_free: no such object %d\n", o);
  }
  int i = (o - objects);
  if(i >= 0 && i < MAX_OBJ){
    if(free_objects[i] == false){
      //printf("object %d already free\n", i);
    }else{
      //printf("freeing obj %d\n", i);
      free_objects[i] = false;
    }
  }else{
    //printf("o is out of bounds (%d)\n", o);
  }
}

#ifdef MMU_TEST
int main(int argc, char **argv)
{
  struct NPC *os[10];
  for(int i = 0;
      i < 10;
      i++){
    os[i] = mmu_alloc(BUG);
  }

  for(int i = 0;
      i < 11;
      i++){
    mmu_free(os[i]);
  }

  for(int i = 0;
      i < 10;
      i++){
    os[i] = mmu_alloc((enum obj_type)(i % 3));
  }

  for(int i = 0;
      i < 12;
      i++){
    mmu_free(os[i]);
  }

  for(int i = 0;
      i < 10;
      i++){
    os[i] = mmu_alloc((enum obj_type)(i % 3));
  }

  for(int i = 0;
      i < 13;
      i++){
    mmu_free(os[i]);
  }
}
#endif
