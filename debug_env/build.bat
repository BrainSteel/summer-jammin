

@echo off


cd obj
gcc -c -I../inc ../src/main.c ../src/gamestate.c ../src/xorshiftstar.c ../src/bitmap.c ../src/pixel.c ../src/common.c ../src/mmu.c

cd ..
gcc obj/main.o obj/common.o obj/xorshiftstar.o obj/pixel.o obj/bitmap.o obj/gamestate.o obj/mmu.o -lgdi32

