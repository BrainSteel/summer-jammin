
#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "stdint.h"

// Meter definition corresponding to screen size
#define SCREEN_WIDTH_METERS 16
#define SCREEN_HEIGHT_METERS 9

//// Velocity and Acceleration
#define MAX_ACCELERATION 0.75
#define MAX_VELOCITY 1

// WIDTH EXTREMITIES
#define MIN_ENTITY_WIDTH 0.1
#define MAX_ENTITY_WIDTH 0.25
#define MIN_PLAYER_WIDTH 0.3
#define MAX_PLAYER_WIDTH 0.3
#define MIN_BUG_WIDTH 0.2
#define MAX_BUG_WIDTH 0.4
#define MIN_BUILDING_WIDTH 0.4
#define MAX_BUILDING_WIDTH 0.75
#define MIN_HAWK_WIDTH 0.3
#define MAX_HAWK_WIDTH 0.5

// HEIGHT EXTREMITITES
#define MIN_ENTITY_HEIGHT 0.1
#define MAX_ENTITY_HEIGHT 0.25
#define MIN_PLAYER_HEIGHT 0.6
#define MAX_PLAYER_HEIGHT 0.6
#define MIN_BUG_HEIGHT 0.2
#define MAX_BUG_HEIGHT 0.4
#define MIN_BUILDING_HEIGHT 0.5
#define MAX_BUILDING_HEIGHT 1
#define MIN_HAWK_HEIGHT 0.3
#define MAX_HAWK_HEIGHT 0.4

// HEALTH -> UNUSED
#define MIN_ENTITY_HEALTH 40
#define MAX_ENTITY_HEALTH 100

// STAMINA
#define INITIAL_STAMINA 100
#define BUG_STAMINA_BOOST 15

// FRAME RATE
#define FRAMES_PER_SECOND 60

// SPAWN
#define SPAWN_RATE .5
#define SPAWN_FRAME_BASE 50
#define SPAWN_FRAME_MIN 70

// INITIAL NPC INFORMATION
#define INITIAL_BUG_COUNT 3
#define INITIAL_HAwk COUNT 3
#define INITIAL_BUILDING_COUNT 3
#define INITIAL_NPC_COUNT 5

#define BOUNDARY_DEMO 6
#define BOUNDARY_EASY 4
#define BOUNDARY_MEDIUM 3
#define BOUNDARY_HARD 2
#define BOUNDARY_IMPOSSIBLE 1

//#define CONSTANT_ACCEL .25
#define CONSTANT_ACCEL .5

#define PLAYER_SAFE_ZONE_X_METER 4

#define NUM_OBJECTS 500

typedef enum PlayerKeys
{
    KeyUp,
    KeyDown,
    KeyLeft,
    KeyRight,
    KeyNum
} PlayerKeys;

typedef struct Input
{
    uint8_t keydown[KeyNum];
    uint8_t mousestate;
    uint16_t mousex, mousey;
} Input;

typedef enum Difficulty
{
    Demo,
    Easy,
    Medium,
    Hard,
    Impossible
} Difficulty;

typedef enum CollisionCorner
{
    TOP_LEFT_INTO_BOTTOM_RIGHT = 1,
    TOP_RIGHT_INTO_BOTTOM_LEFT,
    BOTTOM_LEFT_INTO_TOP_RIGHT,
    BOTTOM_RIGHT_INTO_TOP_LEFT,
} CollisionCorner;

typedef struct Point
{
    float x_meter;
    float y_meter;
} Point;

typedef struct Line
{
    Point p1;
    Point p2;
    float slope;
    float y_intercept;
} Line;

typedef struct Circle
{
    Point center;
    float radius;
} Circle;

typedef struct Rect
{
    float x_meter;
    float y_meter;
    float width;
    float height;
} Rect;

typedef struct Size
{
    float width;
    float height;
} Size;

typedef struct Physics
{
    float accel_x;
    float accel_y;
    float vel_x;
    float vel_y;
} Physics;

typedef struct Entity
{
    Point coords;
    Size size;

    uint8_t health;
    
    Physics physics;
} Entity;

typedef struct Player
{
    Input input;
    uint8_t boundary;
    uint16_t stamina;
    
    Entity entity;

    // texture
    
} Player;

typedef struct Bug
{
    Entity entity;

    // texture
} Bug;

typedef struct Building
{
    Entity entity;

    // texture
} Building;

enum obj_type{ HAWK, BUILDING, BUG, NUM_TYPES };

struct NPC{
  enum obj_type type;
  struct Entity entity;
  /* VulkanBuffer *vbuf; */
};

typedef struct GameState
{
    uint8_t game_over;
    uint8_t running;
    uint8_t paused;

    Difficulty difficulty;
    
    uint64_t logical_frames;
    uint32_t fps;

    float m_to_px_conversion_factor_x;
    float m_to_px_conversion_factor_y;

    Player player;
    struct NPC* npcs[NUM_OBJECTS];

    uint32_t num_bugs;
    uint32_t num_hawks;
    uint32_t num_buildings;
    uint32_t npc_numbers[NUM_TYPES];
    uint32_t num_npcs;
    
} GameState;

void InitializeGameState(GameState* state);
void UpdateGameState(GameState* state);
void DestroyGameState(GameState* state);
Rect GetEntityRectangle(Entity* entity);

#endif //GAMESTATE_H
