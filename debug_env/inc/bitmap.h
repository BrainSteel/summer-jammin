

#ifndef BITMAP_H
#define BITMAP_H

#include "windows.h"
#include "stdint.h"

typedef struct Bitmap
{
    BITMAPINFO bmi;
    
    uint32_t width, height;
    uint8_t bytes_per_pixel;

    void* pixels;
} Bitmap;


void InitializeBitmap(Bitmap* bitmap, int width, int height);



#endif //bitmap->H


