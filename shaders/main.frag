#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#define M_PI 3.1415926535897932384626433832795

layout (std140, binding = 0) uniform model_buffer_val {
    mat4 model;
    mat4 vp;
} model_buffer;

layout (std140, binding = 1) uniform light_buffer_val {
    vec4 pos;
    vec4 intensities;
    vec4 player_light;
} light_buffer;

layout (location = 0) in vec4 pos;
layout (location = 1) in vec4 color;
layout (location = 2) in vec4 normal;

layout (location = 0) out vec4 out_color;

void main() {
    mat3 normal_mat = transpose(inverse(mat3(model_buffer.model)));
    vec3 world_normal = normalize(normal_mat * vec3(normal));

    vec3 world_frag_pos = vec3(model_buffer.model * pos);

    vec3 center_vec = world_frag_pos - vec3(light_buffer.pos);
    center_vec.z = 0;

    vec3 world_light_pos = vec3(light_buffer.pos) + normalize( center_vec ) * light_buffer.pos.w;

    vec3 light_vec = world_light_pos - world_frag_pos;
    vec3 player_light_vec = vec3(light_buffer.player_light)- world_frag_pos;
    if ( length( light_vec ) > length( player_light_vec ) )
    {
        light_vec = player_light_vec;
    }

    float ratio = (M_PI / 2) / light_buffer.intensities.w;
    if ( ratio < 1 )
    {
        ratio = 1;
    }
    float coslight = dot( -light_vec, vec3( 0, 0, -1 ) ) /
                    (length( light_vec ) );

    float angle = acos( coslight );
    if ( angle > light_buffer.intensities.w )
    {
        angle = light_buffer.intensities.w;
    }

    float bright = cos( ratio * angle );

    out_color = vec4(bright * vec3(light_buffer.intensities) * vec3(color), color.a);
}