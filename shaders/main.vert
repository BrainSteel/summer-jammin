#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (std140, binding = 0) uniform model_buffer_val {
    mat4 model;
    mat4 vp;
} model_buffer;

layout (location = 0) in vec4 in_pos;
layout (location = 1) in vec4 in_color;
layout (location = 2) in vec4 in_normal;

layout (location = 0) out vec4 out_pos;
layout (location = 1) out vec4 out_color;
layout (location = 2) out vec4 out_normal;

void main() {
   out_pos = in_pos;
   out_color = in_color;
   out_normal = in_normal;

   gl_Position = model_buffer.vp * model_buffer.model * in_pos;
}