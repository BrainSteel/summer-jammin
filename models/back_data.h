
#ifndef BACK_DATA_H
#define BACK_DATA_H

#include "vulkan_wrapper.h"

#define XYZ1(_x_, _y_, _z_) (_x_), (_y_), (_z_), 1.0f

static ColorVertex back_data[] = 
{
    { XYZ1( -12.0f, -12.0f, 0.0f ), XYZ1( 1.0f, 1.0f, 1.0f ) },
    { XYZ1( -12.0f, 12.0f, 0.0f ), XYZ1( 1.0f, 1.0f, 1.0f ) },
    { XYZ1( 12.0f, 12.0f, 0.0f ), XYZ1( 1.0f, 1.0f, 1.0f ) },

    { XYZ1( -12.0f, -12.0f, 0.0f ), XYZ1( 1.0f, 1.0f, 1.0f ) },
    { XYZ1( 12.0f, 12.0f, 0.0f ), XYZ1( 1.0f, 1.0f, 1.0f ) },
    { XYZ1( 12.0f, -12.0f, 0.0f ), XYZ1( 1.0f, 1.0f, 1.0f ) },

};

#endif