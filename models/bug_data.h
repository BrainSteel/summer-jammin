
#ifndef BUG_DATA_H
#define BUG_DATA_H

#include "vulkan_wrapper.h"

#define XYZ1(_x_, _y_, _z_) (_x_), (_y_), (_z_), 1.0f

#define BUG_EXTRUDE_HEIGHT 1.0f

static ColorVertex bug_front_face[] = 
{
    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
    { XYZ1( -0.5f, 0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },

    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
    { XYZ1( 0.5f, -0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
};

static ColorVertex bug_front_edge[] =
{
    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
    { XYZ1( -0.5f, 0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
    { XYZ1( 0.5f, -0.5f, 0.0f ), XYZ1( 0.0f, 1.0f, 0.0f ) },
};

#endif