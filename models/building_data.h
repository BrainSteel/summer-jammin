
#ifndef BUILDING_DATA_H
#define BUILDING_DATA_H

#include "vulkan_wrapper.h"

#define XYZ1(_x_, _y_, _z_) (_x_), (_y_), (_z_), 1.0f

#define BUILDING_EXTRUDE_HEIGHT 3.0f

static ColorVertex building_front_face[] = 
{
    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.3f, 0.3f, 0.3f ) },
    { XYZ1( -0.5f, 0.5f, 0.0f ), XYZ1( 0.3f, 0.3f, 0.3f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.3f, 0.3f, 0.3f ) },

    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.3f, 0.3f, 0.3f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.3f, 0.3f, 0.3f ) },
    { XYZ1( 0.5f, -0.5f, 0.0f ), XYZ1( 0.3f, 0.3f, 0.3f ) },
};

static ColorVertex building_front_edge[] =
{
    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.25f, 0.25f, 0.25f ) },
    { XYZ1( -0.5f, 0.5f, 0.0f ), XYZ1( 0.25f, 0.25f, 0.25f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.25f, 0.25f, 0.25f ) },
    { XYZ1( 0.5f, -0.5f, 0.0f ), XYZ1( 0.25f, 0.25f, 0.25f ) },
};

#endif