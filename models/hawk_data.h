
#ifndef HAWK_DATA_H
#define HAWK_DATA_H

#include "vulkan_wrapper.h"

#define XYZ1(_x_, _y_, _z_) (_x_), (_y_), (_z_), 1.0f

#define BUG_EXTRUDE_HEIGHT 1.0f

static ColorVertex hawk_front_face[] = 
{
    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
    { XYZ1( -0.5f, 0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },

    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
    { XYZ1( 0.5f, -0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
};

static ColorVertex hawk_front_edge[] =
{
    { XYZ1( -0.5f, -0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
    { XYZ1( -0.5f, 0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
    { XYZ1( 0.5f, 0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
    { XYZ1( 0.5f, -0.5f, 0.0f ), XYZ1( 0.545f, 0.271f, 0.075f ) },
};

#endif