#include <stdlib.h>

#include "renderer.h"
#include "common.h"

struct mlist{
  struct mlist *next;
  struct model *model;
};

struct mlist *queue = NULL;
struct mlist *last = NULL;

static void enqueue(struct model *m)
{
  /* printf("malloc mlist\n"); */
  struct mlist *l = malloc(sizeof(struct mlist));
  /* printf("mlist->model=%d\n", m); */
  l->model = m;
  /* printf("mlist->next=NULL\n"); */
  l->next = NULL;

  if(queue == NULL){
    /* printf("putting this at the start of the queue.\n"); */
    queue = l;
    queue->next = NULL;
    last = queue;
  }else{
    /* printf("putting this at the tail of the queue\n"); */
    last->next = l;
    last = l;
  }
}

static void render_one(struct model *m)
{
  if(m==NULL){
    /* printf("m is null.\n"); */
    return;
  }
  const VkDeviceSize offsets[1] = { 0 };

  uint32_t offset =
    main_give_me_the_offset_plox(m->uniform_offset);
  
  // This descriptor set contains everything we need (position, lighting, etc)
  // other than vertices to render this particular model.
  vkCmdBindDescriptorSets( m->command_buffer,
                           VK_PIPELINE_BIND_POINT_GRAPHICS,
                           m->pipeline_layout,
                           0,
                           1,
                           &m->descriptor_set,
                           1,
                           &offset );

  vkCmdBindVertexBuffers( m->command_buffer,
                          0,
                          1,
                          &m->vbuff.buffer,
                          offsets );

  vkCmdDraw( m->command_buffer,
             m->vcount,
             1,
             0,
             0 );
}

static void render_recurse(struct mlist *list)
{
  /* printf("render recurse\n"); */
  if(list != NULL){
    render_one(list->model);
    if(list->next != NULL){
      render_recurse(list->next);
    }
    free(list);
  }else{
    /* printf("list is empty.\n"); */
  }
}

void render_all(void)
{
  render_recurse(queue);
  queue = NULL;
}

void render_model(struct model *m)
{
  /* printf("totally rendering the model (sike)\n"); */
  enqueue(m);
}

void render_model_pos(struct model *m, Point coords)
{
  if(m == NULL){
    /* printf("MODEL WAS NULL\n"); */
    /* printf("skipping setting pos\n"); */
  }else{
    set_matrix(m->uniform_offset, m->matrix);
    set_entity_position(&m->matrix,
                        coords.x_meter,
                        coords.y_meter,
                        0);
  }
  enqueue(m);
}

void setup_viewport(VkCommandBuffer command_buffer, RenderRect render_area)
{
  // standard stuff
  VkViewport viewport;
  viewport.x = render_area.x;
  viewport.y = render_area.y;
  viewport.height = render_area.height;
  viewport.width = render_area.width;
  viewport.minDepth = 0.0f;
  viewport.maxDepth = 1.0f;
  vkCmdSetViewport( command_buffer, 0, 1, &viewport );

  VkRect2D scissor;
  scissor.extent.width = render_area.width;
  scissor.extent.height = render_area.height;
  scissor.offset.x = render_area.x;
  scissor.offset.y = render_area.y;
  vkCmdSetScissor( command_buffer, 0, 1, &scissor );
}
