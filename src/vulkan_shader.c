#include "common.h"

#include "stdio.h"
#include "stdlib.h"


//#define LOG_LEVEL LOG_QUIET
#include "vulkan_wrapper.h"
#include "platform.h"

// Reads a spv file. The spv pointer is allocated and filled with the
// spv data. Then creates a shader module from this data.
int CreateShaderModuleFromFile( const VkDevice dev,
                                const char* filename, 
                                VkShaderModule* shader, 
                                uint32_t** spv )
{
    size_t shader_size = 0;
    uint32_t* raw_spv = ReadBinaryFileMalloc( filename, &shader_size );
    if ( !spv )
    {
        init_log( "Failed to read shader '%s'\n", filename );
        return 1;
    }
    else
    {
        init_log( "Successfully read %zu bytes for shader '%s'\n", shader_size, filename );
    }
    *spv = raw_spv;

    VkShaderModuleCreateInfo module_create = { 0 };
    module_create.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    module_create.pNext = NULL;
    module_create.flags = 0;
    module_create.codeSize = shader_size;
    module_create.pCode = *spv;
    VkResult check = vkCreateShaderModule( dev, &module_create, NULL, shader );
    if ( check )
    {
        init_log( "Failed to create shader module for shader '%s'\n", filename );
        return 1;
    }
    else
    {
        init_log( "Successfully created shader module for shader '%s'\n", filename );
    }

    return 0;
}

int CreateVertexShader( const VkDevice dev, const char* shader_name, VulkanShader* shader )
{
    char shader_file[256];
    snprintf( shader_file, SIZE_ARRAY( shader_file ), "%s.vert.spv", shader_name );
    if ( CreateShaderModuleFromFile( dev, shader_file, &shader->stage_create.module, &shader->spv ) )
    {
        return 1;
    }


    shader->stage_create.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shader->stage_create.pNext = NULL;
    shader->stage_create.pSpecializationInfo = NULL;
    shader->stage_create.flags = 0;
    shader->stage_create.stage = VK_SHADER_STAGE_VERTEX_BIT;
    shader->stage_create.pName = shader_name;

    return 0;
}

int CreateFragmentShader( const VkDevice dev, const char* shader_name, VulkanShader* shader )
{
    char shader_file[256];
    snprintf( shader_file, SIZE_ARRAY( shader_file ), "%s.frag.spv", shader_name );

    if( CreateShaderModuleFromFile( dev, shader_file, &shader->stage_create.module, &shader->spv ) )
    {
        return 1;
    }

    shader->stage_create.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shader->stage_create.pNext = NULL;
    shader->stage_create.pSpecializationInfo = NULL;
    shader->stage_create.flags = 0;
    shader->stage_create.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    shader->stage_create.pName = shader_name;

    return 0;
}

void FreeShader( const VkDevice dev, VulkanShader* shader )
{
    free( shader->spv );
    vkDestroyShaderModule( dev, shader->stage_create.module, NULL );
}
