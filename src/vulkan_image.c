#include "stdio.h"
#include "stdlib.h"

//#define LOG_LEVEL LOG_QUIET
#include "vulkan_wrapper.h"

// Creates the depth buffer image, allocates memory for it, and creates a view.
int CreateDepthImage( const VkPhysicalDevice phys_dev,
                      const VkDevice dev,
                      uint32_t width,
                      uint32_t height,
                      VulkanImage* depth_image )
{
    VkImageCreateInfo depth_buffer_create;
    VkFormatProperties depth_format_properties;
    vkGetPhysicalDeviceFormatProperties( phys_dev, VK_FORMAT_D16_UNORM, &depth_format_properties );
    if ( depth_format_properties.linearTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT )
    {
        init_log( "Chose linear tiling for depth buffer.\n" );
        depth_buffer_create.tiling = VK_IMAGE_TILING_LINEAR;
    }
    else if ( depth_format_properties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT )
    {
        init_log( "Chose optimal tiling for depth buffer.\n" );
        depth_buffer_create.tiling = VK_IMAGE_TILING_OPTIMAL;
    }
    else
    {
        init_log( "Failed to use D16 UNORM format because it is unsupported.\n" );
        return 1;
    }
    depth_image->format = VK_FORMAT_D16_UNORM;

    depth_buffer_create.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    depth_buffer_create.pNext = NULL;
    depth_buffer_create.imageType = VK_IMAGE_TYPE_2D;
    depth_buffer_create.format = VK_FORMAT_D16_UNORM;
    depth_buffer_create.extent.width = width;
    depth_buffer_create.extent.height = height;
    depth_buffer_create.extent.depth = 1;
    depth_buffer_create.mipLevels = 1;
    depth_buffer_create.arrayLayers = 1;
    depth_buffer_create.samples = 1;
    depth_buffer_create.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depth_buffer_create.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    depth_buffer_create.queueFamilyIndexCount = 0;
    depth_buffer_create.pQueueFamilyIndices = NULL;
    depth_buffer_create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    depth_buffer_create.flags = 0;

    VkResult check = vkCreateImage( dev, &depth_buffer_create, NULL, &depth_image->image );
    if ( check )
    {
        init_log( "Failed to create Depth Buffer image.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created depth buffer image.\n" );
    }

    VkPhysicalDeviceMemoryProperties memory_properties;
    vkGetPhysicalDeviceMemoryProperties( phys_dev, &memory_properties );

    VkMemoryRequirements memory_requirements;
    vkGetImageMemoryRequirements( dev, depth_image->image, &memory_requirements );

    VkMemoryAllocateInfo mem_alloc = { 0 };
    mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mem_alloc.pNext = NULL;
    mem_alloc.allocationSize = memory_requirements.size;
    check = GetMemoryTypeFromDeviceProperties( &memory_properties, 
                                               memory_requirements.memoryTypeBits,
                                               0, 
                                               &mem_alloc.memoryTypeIndex );
    if ( check )
    {
        init_log( "Failed to get Memory Type from physical device properties.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully found memory type with index %u from device properties.\n",
            mem_alloc.memoryTypeIndex );
    }

    check = vkAllocateMemory( dev, &mem_alloc, NULL, &depth_image->mem );
    if ( check )
    {
        init_log( "Failed to allocate depth buffer memory.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully allocated depth buffer memory.\n" );
    }

    check = vkBindImageMemory( dev, depth_image->image, depth_image->mem, 0 );
    if ( check )
    {
        init_log( "Failed to bind depth buffer image memory.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully bound image memory.\n" );
    }

    VkImageViewCreateInfo depth_image_view_create = { 0 };
    depth_image_view_create.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    depth_image_view_create.pNext = NULL;
    depth_image_view_create.image = depth_image->image;
    depth_image_view_create.format = VK_FORMAT_D16_UNORM;
    depth_image_view_create.components.r = VK_COMPONENT_SWIZZLE_R;
    depth_image_view_create.components.g = VK_COMPONENT_SWIZZLE_G;
    depth_image_view_create.components.b = VK_COMPONENT_SWIZZLE_B;
    depth_image_view_create.components.a = VK_COMPONENT_SWIZZLE_A;
    depth_image_view_create.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    depth_image_view_create.subresourceRange.baseMipLevel = 0;
    depth_image_view_create.subresourceRange.levelCount = 1;
    depth_image_view_create.subresourceRange.baseArrayLayer = 0;
    depth_image_view_create.subresourceRange.layerCount = 1;
    depth_image_view_create.viewType = VK_IMAGE_VIEW_TYPE_2D;
    depth_image_view_create.flags = 0;

    check = vkCreateImageView( dev, &depth_image_view_create, NULL, &depth_image->view );
    if ( check )
    {
        init_log( "Failed to create image view for depth buffer image.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created image view for depth buffer.\n" );
    }

    return 0;
}

void FreeImage( const VkDevice dev, VulkanImage* image )
{
    vkDestroyImageView( dev, image->view, NULL );
    vkFreeMemory( dev, image->mem, NULL );
    vkDestroyImage( dev, image->image, NULL );
}
