#include <stdio.h>
#include <stdlib.h>

#include "../inc/matrix.h"
#include "../inc/mmu.h"
#include "../inc/common.h"

#define Q_MAX 1000

void q_push(enum entity, int);
int q_peek(enum entity);
int q_pop(enum entity);

// rows, cols
// rows are entity, cols are index
int q_store[ENTITY_COUNT][Q_MAX];

// index points to the current head.
int q_index[ENTITY_COUNT];

void q_init() {
  for(int i; i < ENTITY_COUNT; i++) {
    q_index[i] = -1;
  }
}

int q_peek(enum entity key) {
  if (q_index[key] <= -1) {
    return -1;
    /* return NULL; */
  }

  /* logg("peek [%d][%d]", key, q_index[key]); */
  return q_store[key][q_index[key]];
}

void q_push(enum entity key, int item) {
  /* logg("push [%d][%d]", key, q_index[key]); */
  q_store[key][++q_index[key]] = item;
}

int q_pop(enum entity key) {
  /* logg("pop"); */
  int ret = q_peek(key);
  q_index[key]--;
  return ret;
}

// gcc -lm -Dtest queue.c && ./a.out
#ifdef test
int main(int argc, char **argv)
{
  q_init();
  q_push(BUG, 7);
  q_push(BUG, 5);
  q_push(BUG, 4);
  logg("%d", q_peek(BUG));
  logg("%d", q_pop(BUG));
  logg("%d", q_pop(BUG));
  logg("%d", q_pop(BUG));
}
#endif
