#include "stdio.h"
#include "stdlib.h"

//#define LOG_LEVEL LOG_QUIET
#include "vulkan_wrapper.h"
#include "listmalloc.h"

void WaitForFenceIndefinite( const VkDevice dev,
                             const VkFence wait_for )
{
    VkResult check;
    do
    {
        // Timeout is in nanoseconds.
        check = vkWaitForFences( dev, 1, &wait_for, VK_TRUE, (uint64_t)1e9 );
    } while ( check == VK_TIMEOUT );
}

int GetMemoryTypeFromDeviceProperties( const VkPhysicalDeviceMemoryProperties* properties, 
                                       uint32_t type_bits,
                                       VkFlags req_mask, 
                                       uint32_t* type_index )
{
    // Search memtypes to find first index with those properties
    for ( uint32_t i = 0; i < properties->memoryTypeCount; i++ )
    {
        if ( (type_bits & 1) == 1 )
        {
            // Type is available, does it match user properties?
            if ( (properties->memoryTypes[i].propertyFlags & req_mask) == req_mask )
            {
                *type_index = i;
                return 0;
            }
        }
        type_bits >>= 1;
    }

    // No memory types matched, return failure
    return 1;
}

void UpdateDescriptorSet( const VkDevice dev, 
                          const VkDescriptorSet descriptor_set, 
                          const VulkanBuffer* uniform_buf,
                          uint32_t destination_binding )
{
    VkWriteDescriptorSet writes[1] = { 0 };
    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].pNext = NULL;
    writes[0].dstSet = descriptor_set;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = uniform_buf->is_dynamic ? VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC : VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writes[0].pBufferInfo = &uniform_buf->descriptor_info;
    writes[0].dstArrayElement = 0;
    writes[0].dstBinding = destination_binding;

    vkUpdateDescriptorSets( dev, 1, writes, 0, NULL );
}
