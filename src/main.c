#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define LOG_LEVEL LOG_VERBOSE
#include <vulkan_wrapper.h>
#include <platform.h>

#include "listmalloc.h"
#include "common.h"
#include "matrix.h"
#include "gamestate.h"
#include "model.h"

#include "bat_data.h"
#include "bug_data.h"
#include "back_data.h"

#include "renderer.h"

char error_buf[256];

struct model mback;
struct model mbat;
struct model mbug;

// This struct contains all vulkan data needed to render a cube.
static struct
{
  // Vulkan initialization data, roughly in chronological order
  // Everything here is initialized and destroyed once.
  VkApplicationInfo app_info;
  VkInstance inst;

  VkPhysicalDevice* gpus;
  uint32_t gpu_count;

  VkSurfaceKHR window_surface;

  VkQueueFamilyProperties* queue_families;
  uint32_t queue_count;
  uint32_t graphics_queue_index;
  uint32_t present_queue_index;

  VkDevice dev;

  VkCommandPool command_pool;
  VkCommandBuffer command_buffer;

  VulkanSwapchain swapchain;

  VkQueue graphics_queue;
  VkQueue present_queue;

  // Honestly not entirely sure what these are
  // but we have to build them.
  VkDescriptorSetLayout descriptor_layout;
  VkDescriptorPool descriptor_pool;
  VkDescriptorSet descriptor_set;
  VkPipelineLayout pipeline_layout;

  VulkanImage depth_image;

  VkRenderPass render_pass;

  // Same number of these as swapchain images
  VkFramebuffer* framebuffers;

  VulkanShader fragment_shader;
  VulkanShader vertex_shader;

  VkPipeline pipeline;
} vk_info;

static GameState state;

void InitializeVulkanData( const WindowInfo* window );
void DestroyVulkanData( );

void InitializeRenderingData( );
void DestroyRenderingData();

void FullRenderSequence( WindowInfo* window, RenderRect render_area )
{
  VkSemaphore acquire_sem;
  uint32_t acquired_image;
  AcquireSwapchainImage( vk_info.dev,
                         &vk_info.swapchain,
                         &acquired_image,
                         &acquire_sem );

  BeginRenderRecording( vk_info.dev,
                        vk_info.render_pass,
                        vk_info.framebuffers[acquired_image],
                        vk_info.command_buffer,
                        render_area );

  vkCmdBindPipeline( vk_info.command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vk_info.pipeline );

  setup_viewport(vk_info.command_buffer, render_area);
  render_all();

  EndRenderRecording( vk_info.command_buffer );

  VkFence draw_fence;
  SubmitQueue( vk_info.dev,
               vk_info.graphics_queue,
               &vk_info.command_buffer,
               1,
               &acquire_sem,
               &draw_fence );

  PresentToDisplay( vk_info.dev,
                    vk_info.present_queue,
                    &vk_info.swapchain,
                    acquired_image,
                    draw_fence );

  vkDestroySemaphore( vk_info.dev, acquire_sem, NULL );
  vkDestroyFence( vk_info.dev, draw_fence, NULL );

  DoWindowRenderUpdate( window );
}

void set_entity_position(Matrix4 *m,
                         float x,
                         float y,
                         float z)
{
  if(m == NULL){
    /* printf("MODEL WAS NULL\n"); */
  }else{
    m->vec[3].x = x - 8;
    m->vec[3].y = -1 * (y - 4.5);
    m->vec[3].z = z;
  }
}

void set_entity_rotation(Matrix4 *m,
                         float a,
                         float b,
                         float t)
{
  /* generate the rotation of the bat depending on the inputs */
  Matrix4 rotation = GetRotation(a, b, t);
  *m = Mult4(*m, rotation);
}

void UpdatePlayerModel( const Input* input,
                        Matrix4 *model,
                        float frames_passed )
{
  *model = GetDiagonalMatrix(1.0f);
  set_entity_position(model,
                      state.player.entity.coords.x_meter,
                      state.player.entity.coords.y_meter,
                      0);
  set_entity_rotation(model, 0.0f, M_PI / 6 *
                                 (input->keydown[KeyDown] -
                                  input->keydown[KeyUp]),
                      -M_PI / 2.0f);

}

void UpdateModels( const Entity* entities, Matrix4* models, uint32_t count )
{

}

static Matrix4 buffer_contents[2 * NUM_OBJECTS];
void WriteModelsToBuffer( VulkanBuffer* dest, Matrix4* models, uint32_t count )
{
    uint32_t i;
    for ( i = 0; i < count; i++ )
    {
        buffer_contents[2 * i] = models[i];
    }

    WriteToUniformBuffer( vk_info.dev, dest, 2 * sizeof( Matrix4 ), count, buffer_contents );
}

int main(int argc, char **argv)
{
  WindowInfo* window = InitializeWindowInfo( "Spoopy Spots", 1280, 720 );
  InitializeVulkanData( window );
  InitializeRenderingData( );
  RenderRect full;
  full.x = 0;
  full.y = 0;
  full.width = GetWidth( window );
  full.height = GetHeight( window );

  CreateVertexBuffer( vk_info.gpus[0], vk_info.dev, back_data, SIZE_ARRAY( back_data ), &mback.vbuff );
  mback.vcount = SIZE_ARRAY( back_data );
  mback.command_buffer = vk_info.command_buffer;
  mback.pipeline_layout = vk_info.pipeline_layout;
  mback.descriptor_set = vk_info.descriptor_set;
  mback.uniform_offset = 2 * state.rendering.position.alignment;
  const uint64_t tickfreq = GetTickFrequency();
  uint32_t renders_this_second = 0;
  uint64_t last = GetTicks();

  InitializeGameState( &state );
  FullRenderSequence( window, full );

  state.rendering.light_data[LIGHT_X] = state.rendering.model_matrices[0].vec[3].x;
  state.rendering.light_data[LIGHT_Y] = state.rendering.model_matrices[0].vec[3].y;
  state.rendering.light_data[LIGHT_RAD] = 0;

  while ( !state.player.input.quit )
    {
      uint64_t start_ticks = GetTicks( );

      ProcessEvents( &state.player.input );
      if ( state.player.input.quit )
        {
          break;
        }

      UpdatePlayerModel( &state.player.input,
                         &state.rendering.model_matrices[0],
                         1.0f );
      state.rendering.light_data[LIGHT_RAD] += 0.3;
      state.rendering.light_data[LIGHT_PLAYER_X] = state.rendering.model_matrices[0].vec[3].x;
      state.rendering.light_data[LIGHT_PLAYER_Y] = state.rendering.model_matrices[0].vec[3].y;

      if ( state.rendering.light_data[LIGHT_RAD] > 1.5 * SCREEN_WIDTH_METERS )
      {
          state.rendering.light_data[LIGHT_RAD] = 0;
          state.rendering.light_data[LIGHT_X] = state.rendering.light_data[LIGHT_PLAYER_X];
          state.rendering.light_data[LIGHT_Y] = state.rendering.light_data[LIGHT_PLAYER_Y];
      }

      UpdateGameState(&state);
      state.logical_frames++;

      // Enter a rendering loop until
      // enough time has passed to necessitate another logical update
      uint64_t ticks_passed = GetTicks( ) - start_ticks;
      float frames_passed = (ticks_passed * FRAMES_PER_SECOND) / (float)tickfreq;
      while ( frames_passed < 1.0f )
        {
          //Matrix4 old = state.rendering.model_matrices[0];
          //UpdatePlayerModel( &state.player.input, &state.rendering.model_matrices[0], frames_passed );
          WriteModelsToBuffer( &state.rendering.position,
                               state.rendering.model_matrices,
                               NUM_OBJECTS);
          WriteToVulkanBuffer( vk_info.dev,
                               &state.rendering.lights,
                               state.rendering.light_data,
                               sizeof( state.rendering.light_data ) );
          //state.rendering.model_matrices[0] = old;
          render_model(&mback);
          render_model(&mbat);
          render_model(&mbug);
          FullRenderSequence( window, full );

          uint64_t now = GetTicks( );
          ticks_passed = now - start_ticks;
          frames_passed = (ticks_passed * FRAMES_PER_SECOND) / (float)tickfreq;

          if ( now - last > tickfreq )
            {
              state.fps = renders_this_second;
              renders_this_second = 0;
              last = now;
            }

          renders_this_second++;
        }
    }

  printf( "Cleaning up...\n" );

  DestroyRenderingData( );
  DestroyVulkanData( );

  printf( "Done!\n" );

  return 0;
}

void InitializeVulkanData( const WindowInfo* window )
{
  InitializeInstance( &vk_info.app_info, &vk_info.inst, "Test Application" );
  vk_info.gpus = GetPhysicalDevices( vk_info.inst, &vk_info.gpu_count );
  CreateWindowSurface( vk_info.inst, window, &vk_info.window_surface );
  vk_info.queue_families = GetQueueFamilies( vk_info.gpus[0],
                                             vk_info.window_surface,
                                             &vk_info.graphics_queue_index,
                                             &vk_info.present_queue_index,
                                             &vk_info.queue_count );

  InitializeLogicalDevice( vk_info.gpus[0], vk_info.graphics_queue_index, &vk_info.dev );
  InitializeCommandPool( vk_info.dev,
                         vk_info.graphics_queue_index,
                         &vk_info.command_pool,
                         &vk_info.command_buffer,
                         1 );

  uint32_t width = GetWidth( window );
  uint32_t height = GetHeight( window );
  InitializeSwapchain( vk_info.gpus[0],
                       vk_info.dev,
                       vk_info.window_surface,
                       width,
                       height,
                       vk_info.graphics_queue_index,
                       vk_info.present_queue_index,
                       &vk_info.swapchain );

  InitializeGraphicsAndPresentQueues( vk_info.dev,
                                      vk_info.graphics_queue_index,
                                      vk_info.present_queue_index,
                                      &vk_info.graphics_queue,
                                      &vk_info.present_queue );

  InitializePipelineLayoutAndDescriptorSets( vk_info.dev,
                                             &vk_info.descriptor_layout,
                                             &vk_info.descriptor_pool,
                                             &vk_info.descriptor_set,
                                             &vk_info.pipeline_layout );

  CreateDepthImage( vk_info.gpus[0],
                    vk_info.dev,
                    width,
                    height,
                    &vk_info.depth_image );

  InitializeRenderPass( vk_info.dev,
                        &vk_info.depth_image,
                        vk_info.swapchain.format,
                        &vk_info.render_pass );

  vk_info.framebuffers = malloc( sizeof( VkFramebuffer ) * vk_info.swapchain.count );
  InitializeFramebuffers( vk_info.dev,
                          vk_info.render_pass,
                          &vk_info.depth_image,
                          &vk_info.swapchain,
                          width,
                          height,
                          vk_info.framebuffers );

  CreateVertexShader( vk_info.dev,
                      "main",
                      &vk_info.vertex_shader );

  CreateFragmentShader( vk_info.dev,
                        "main",
                        &vk_info.fragment_shader );

  VulkanShader shaders[] = { vk_info.vertex_shader, vk_info.fragment_shader };

  InitializePipeline( vk_info.dev,
                      vk_info.pipeline_layout,
                      vk_info.render_pass,
                      shaders,
                      SIZE_ARRAY( shaders ),
                      &vk_info.pipeline );
}

void DestroyVulkanData( )
{
  vkDestroyPipeline( vk_info.dev, vk_info.pipeline, NULL );
  FreeShader( vk_info.dev, &vk_info.fragment_shader );
  FreeShader( vk_info.dev, &vk_info.vertex_shader );
  int i;
  for ( i = 0; i < vk_info.swapchain.count; i++ )
    {
      vkDestroyFramebuffer( vk_info.dev, vk_info.framebuffers[i], NULL );
    }
  free( vk_info.framebuffers );
  vkDestroyRenderPass( vk_info.dev, vk_info.render_pass, NULL );
  FreeImage( vk_info.dev, &vk_info.depth_image );
  vkDestroyPipelineLayout( vk_info.dev, vk_info.pipeline_layout, NULL );
  vkFreeDescriptorSets( vk_info.dev, vk_info.descriptor_pool, 1, &vk_info.descriptor_set );
  vkDestroyDescriptorPool( vk_info.dev, vk_info.descriptor_pool, NULL );
  vkDestroyDescriptorSetLayout( vk_info.dev, vk_info.descriptor_layout, NULL );
  for ( i = 0; i < vk_info.swapchain.count; i++ )
    {
      vkDestroyImageView( vk_info.dev, vk_info.swapchain.views[i], NULL );
      vkDestroyImage( vk_info.dev, vk_info.swapchain.images[i], NULL );
    }
  vkDestroySwapchainKHR( vk_info.dev, vk_info.swapchain.swapchain, NULL );
  vkFreeCommandBuffers( vk_info.dev, vk_info.command_pool, 1, &vk_info.command_buffer );
  vkDestroyCommandPool( vk_info.dev, vk_info.command_pool, NULL );
  vkDestroyDevice( vk_info.dev, NULL );
  free( vk_info.queue_families );
  free( vk_info.gpus );
  vkDestroyInstance( vk_info.inst, NULL );
}

void init_descriptors(void)
{
    CreateUniformBuffer( vk_info.gpus[0],
                       vk_info.dev,
                       sizeof( Matrix4 ) * 2,
                       NUM_OBJECTS,
                       &state.rendering.position );

    UpdateDescriptorSet( vk_info.dev,
                       vk_info.descriptor_set,
                       &state.rendering.position,
                       0 );

    CreateUniformBuffer( vk_info.gpus[0],
                         vk_info.dev,
                         8 * sizeof( float ),
                         1,
                         &state.rendering.lights );

    state.rendering.light_data[LIGHT_Z] = 100.0f;
    state.rendering.light_data[LIGHT_R] = 2.0f;
    state.rendering.light_data[LIGHT_G] = 2.0f;
    state.rendering.light_data[LIGHT_B] = 2.0f;
    state.rendering.light_data[LIGHT_ANG] = M_PI / 240;
    state.rendering.light_data[LIGHT_PLAYER_Z] = state.rendering.light_data[LIGHT_Z];

    WriteToVulkanBuffer( vk_info.dev,
                         &state.rendering.lights,
                         state.rendering.light_data,
                         sizeof( state.rendering.light_data ) );

    UpdateDescriptorSet( vk_info.dev,
                         vk_info.descriptor_set,
                         &state.rendering.lights,
                         1 );
}

void create_model_post_extruded(struct model *m,
                                ColorVertex *verts,
                                uint32_t offset)
{
  CreateVertexBuffer(vk_info.gpus[0],
                     vk_info.dev,
                     verts,
                     m->vcount,
                     &m->vbuff);
  free(verts);
  m->command_buffer = vk_info.command_buffer;
  m->pipeline_layout = vk_info.pipeline_layout;
  m->descriptor_set = vk_info.descriptor_set;
  m->uniform_offset = offset;
}

void create_model(struct model *m,
                  ColorVertex *front_face, uint32_t face_len,
                  ColorVertex *front_edge, uint32_t edge_len,
                  float extrude_height,
                  uint32_t offset)
{
  ColorVertex *vertices = ExtrudeModel(front_face, face_len,
                                       front_edge, edge_len,
                                       extrude_height, &m->vcount);
  CreateVertexBuffer(vk_info.gpus[0],
                     vk_info.dev,
                     vertices,
                     m->vcount,
                     &m->vbuff);
  free(vertices);
  m->command_buffer = vk_info.command_buffer;
  m->pipeline_layout = vk_info.pipeline_layout;
  m->descriptor_set = vk_info.descriptor_set;
  m->uniform_offset = offset;
}

void init_view(void)
{
    state.rendering.model_matrices[0] = GetRotation( 0.0f, 0.0f, -M_PI / 2 );
    state.rendering.model_matrices[1] = GetTranslation( 1.0f, 0.0f, 0.0f );
    state.rendering.model_matrices[2] = GetTranslation( 0.0f, 0.0f, -3.0f );
    Matrix4 projection = GetProjection2D( -SCREEN_WIDTH_METERS / 2.0f, SCREEN_WIDTH_METERS / 2.0f, -SCREEN_HEIGHT_METERS / 2.0f, SCREEN_HEIGHT_METERS / 2.0f, 0.1f, 30.0f );
    Matrix4 view = LookAt( (Vector3) { 0.0f, 0.0f, 20.0f },
                           (Vector3) { 0.0f, 0.0f, 0.0f },
                           (Vector3) { 0.0f, 1.0f, 0.0f } );

    Matrix4 clip;
    clip.vec[0] = (Vector4) { 1.0f, 0.0f, 0.0f, 0.0f };
    clip.vec[1] = (Vector4) { 0.0f, -1.0f, 0.0f, 0.0f };
    clip.vec[2] = (Vector4) { 0.0f, 0.0f, 0.5f, 0.5f };
    clip.vec[3] = (Vector4) { 0.0f, 0.0f, 0.0f, 1.0f };

    state.rendering.vp = Mult4( Mult4( clip, projection ), view );

    uint32_t i;
    for ( i = 0; i < NUM_OBJECTS; i++ )
    {
        buffer_contents[2 * i + 1] = state.rendering.vp;
    }
}

void InitializeRenderingData( )
{
  init_view();
  init_descriptors();

  create_model(&mbat,
               bat_front_face, SIZE_ARRAY(bat_front_face),
               bat_front_edge, SIZE_ARRAY(bat_front_edge),
               BAT_EXTRUDE_HEIGHT,
               0);

  create_model(&mbug,
               bug_front_face, SIZE_ARRAY(bug_front_face),
               bug_front_edge, SIZE_ARRAY(bug_front_edge),
               BUG_EXTRUDE_HEIGHT,
               state.rendering.position.alignment );

}

void DestroyRenderingData()
{
  FreeVulkanBuffer( vk_info.dev, &state.rendering.lights );
  FreeVulkanBuffer( vk_info.dev, &state.rendering.position );
}

void delete_model( struct model* m )
{
    FreeVulkanBuffer( vk_info.dev, &m->vbuff );
}

uint32_t main_give_me_the_offset_plox(uint32_t in_off)
{
  return in_off * state.rendering.position.alignment;
}

void set_matrix(uint32_t c, Matrix4 m)
{
  state.rendering.model_matrices[c] = m;
}
